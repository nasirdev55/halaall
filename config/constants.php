<?php

return [
	'pagination' => [
		'per_page' => 10
	],
	'dashboard' => [
		'categories' => 10,
		'popular' => [
			'restaurants' => 10,
			'pickups' => 10,
		]
	]
];