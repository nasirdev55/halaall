<?php

use Illuminate\Support\Facades\Route;



Route::group(['namespace' => 'Admin', 'as' => 'admin.'], function () {
    /*authentication*/
    Route::group(['namespace' => 'Auth', 'prefix' => 'auth', 'as' => 'auth.'], function () {
        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@submit');
        Route::get('logout', 'LoginController@logout')->name('logout');
    });
    /*authentication*/

    Route::group(['middleware' => ['admin']], function () {
        Route::get('/', 'SystemController@dashboard')->name('dashboard');
        Route::group(['prefix' => 'restaurants', 'as' => 'restaurants.'], function(){
            Route::group(['prefix' => 'request'], function(){
                Route::get('/', 'RestaurantController@requests')->name('request');
                Route::get('{restaurant}/update-status/{status}', 'RestaurantController@requestUpdateStatus')->name('request.update.status');
                Route::post('search', 'RestaurantController@requestSearch')->name('request.search');
            });
            
            Route::post('search', 'RestaurantController@search')->name('search');
        });
        Route::resource('restaurants', 'RestaurantController');
        
        Route::group(['prefix' => 'categories', 'as' => 'categories.'], function(){
            Route::get('{category}/status/{status}', 'CategoryController@updateStatus')->name('status');
            Route::post('search', 'CategoryController@search')->name('search');
        });
        Route::resource('categories', 'CategoryController');

        Route::group(['prefix' => 'products', 'as' => 'products.'], function(){
            Route::post('search', 'ProductController@search')->name('search');
            Route::get('{product}/update-status/{status}', 'ProductController@updateStatus')->name('update.status');
            Route::group(['prefix' => 'request'], function(){
                Route::get('/', 'ProductController@requests')->name('request');
                Route::get('{product}/update-status/{status}', 'ProductController@requestUpdateStatus')->name('request.update.status');
                Route::post('search', 'ProductController@requestSearch')->name('request.search');
            });
        });
        Route::resource('products', 'ProductController');

        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
            Route::get('/', 'SystemController@settings')->name('show');
            Route::post('profile', 'SystemController@profile')->name('profile');
            Route::post('update-password', 'SystemController@updatePassword')->name('update.password');
            // Route::get('/get-restaurant-data', 'SystemController@restaurant_data')->name('get-restaurant-data');
        });

        // Route::group(['prefix' => 'category', 'as' => 'category.'], function () {
        //     Route::get('add', 'CategoryController@index')->name('add');
        //     Route::get('add-sub-category', 'CategoryController@sub_index')->name('add-sub-category');
        //     Route::get('add-sub-sub-category', 'CategoryController@sub_sub_index')->name('add-sub-sub-category');
        //     Route::post('store', 'CategoryController@store')->name('store');
        //     Route::get('edit/{id}', 'CategoryController@edit')->name('edit');
        //     Route::post('update/{id}', 'CategoryController@update')->name('update');
        //     Route::post('store', 'CategoryController@store')->name('store');
        //     Route::get('status/{id}/{status}', 'CategoryController@status')->name('status');
        //     Route::delete('delete/{id}', 'CategoryController@delete')->name('delete');
        //     Route::post('search', 'CategoryController@search')->name('search');
        // });
        // Route::group(['prefix' => 'branch', 'as' => 'branch.'], function () {
        //     Route::get('add-new', 'BranchController@index')->name('add-new');
        //     Route::post('store', 'BranchController@store')->name('store');
        //     Route::get('edit/{id}', 'BranchController@edit')->name('edit');
        //     Route::post('update/{id}', 'BranchController@update')->name('update');
        //     Route::delete('delete/{id}', 'BranchController@delete')->name('delete');
        // });
    });
});
