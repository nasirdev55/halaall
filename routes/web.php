<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['namespace' => 'Web', 'as' => 'web.'], function() {
    Route::group(['middleware' => 'guest'], function() {
        Route::get('/', 'LandingController@index')->name('landing');
        Route::get('restaurant/detail', 'RestaurantController@show')->name('restaurant.details');
        Route::get('search', 'SearchController@search')->name('search');
        Route::get('restaurant/search', 'SearchController@restaurantSearch')->name('restaurant.search');
//        Custom Code
//        end Code

        Route::get('aboutus', 'PageController@aboutUs')->name('aboutus');
        Route::get('privacy', 'PageController@privacy')->name('privacy');
        Route::get('faq', 'PageController@faq')->name('faq');
        Route::get('terms', 'PageController@terms')->name('terms');
    });

    Route::group(['middleware' => 'auth'], function() {
        Route::get('/home', 'HomeController@index')->name('home');
    });
});

Route::get('category/{detail}','Custom\CategoryController@categoryDetail')->name('categoryDetail');
Route::get('product/{detail}','Custom\ProductController@productDetail')->name('productDetail');
Route::post('cart/{product_id}','Custom\CartController@cart')->name('productCart');
