<?php

use Illuminate\Support\Facades\Route;


Route::group(['namespace' => 'Restaurant', 'as' => 'restaurant.'], function () {
    /*authentication*/
    Route::group(['namespace' => 'Auth', 'prefix' => 'auth', 'as' => 'auth.'], function () {
        Route::get('login', 'LoginController@login')->name('login');
        Route::post('login', 'LoginController@submit');
        Route::get('logout', 'LoginController@logout')->name('logout');
        Route::post('register', 'RegisterController')->name('register');
    });
    /*authentication*/

    Route::group(['middleware' => ['restaurant', 'active_or_approved']], function () {
        Route::get('/', 'SystemController@dashboard')->name('dashboard');

        Route::resource('categories', 'CategoryController');
        Route::group(['prefix' => 'categories', 'as' => 'categories.'], function(){
            Route::get('{category}/status/{status}', 'CategoryController@updateStatus')->name('status');
        });
        
        Route::resource('products', 'ProductController');
        Route::resource('deals', 'DealController');

        Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
            Route::get('/', 'SystemController@settings')->name('show');
            Route::post('profile', 'SystemController@profile')->name('profile');
            Route::post('update-password', 'SystemController@updatePassword')->name('update.password');
            // Route::get('/get-restaurant-data', 'SystemController@restaurant_data')->name('get-restaurant-data');
        });
    });
});
