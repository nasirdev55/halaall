<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantAccount extends Model
{
    protected $fillable = [
        'restaurant_id',
        'first_name',
        'last_name',
        'card_number',
        'expiry',
        'security_code'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
