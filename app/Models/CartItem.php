<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    protected $fillable = [
        'cart_id',
        'item_id',
        'item_type',
        'unit_price',
        'quantity'
    ];

    protected $hidden = [
        'item_id',
        'item_type',
        'created_at',
        'updated_at',
    ];
}
