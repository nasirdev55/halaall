<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RestaurantTiming extends Model
{
    protected $fillable = [
        'restaurant_id',
        'start_time',
        'end_time'
    ];
}
