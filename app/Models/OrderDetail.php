<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
        'order_id',
        'delivery_charges',
        'tax_percent',
        'tax',
        'commision_percent',
        'total_commision',
        'sub_total',
        'total',
        'payment_id',
        'payment_status',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
