<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Product extends Model
{
    protected $fillable = [
        'category_id',
        'restaurant_id',
        'name',
        'description',
        'price',
        'is_featured',
        'is_available',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'is_featured' => 'boolean',
        'is_available' => 'boolean'
    ];

    public static $statuses = array(
       'pending'  => 'Pending',
       'approved' => 'Approved',
       'rejected' => 'Rejected',
       'enabled'  => 'Enabled',
       'disabled' => 'Disabled'
    );

    protected $with = 'images';

    protected static function boot()
    {
        parent::boot();

        // static::addGlobalScope(new ActiveScope);
    }

    public function images()
    {
        return $this->hasMany('App\Models\ProductImage');
    }

    public function singleImage()
    {
        $image = $this->images()->latest()->first();
        // dd($image);
        return ($image)?asset('storage/product/'.$image->image):null;
    }

    static function pending()
    {
        return self::where('status', 'pending');
    }

    static function notPending()
    {
        return self::where('status', '!=', 'pending');
    }

    public function scopeFeaturedProducts($query)
    {
        return $query->where('is_featured', 1)->where('status', 'approved');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    /**
     * Get all of the product's cart.
     */
    public function categories()
    {
        return $this->morphMany(Cart::class, 'cartable');
    }
}
