<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $fillable = [
        'order_id',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];


}
