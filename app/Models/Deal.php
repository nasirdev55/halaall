<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Deal extends Model
{
    protected $fillable = [
        'restaurant_id',
        'name',
        'description',
        'price',
        'delivery_time',
        'image',
        'status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();
 
        static::addGlobalScope(new ActiveScope);
    }

    public function getImageAttribute($image)
    {
        return asset('storage/deal/'.$image);
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    /**
     * Get all of the Deal's cart.
     */
    public function carts()
    {
        return $this->morphMany(Cart::class, 'cartable');
    }
}
