<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Restaurant extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'latitude',
        'longitude',
        'address',
        'status',
        'logo',
        'delivery_time',
        'delivery_charges'
    ];

    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public static $statuses = array(
       'pending'  => 'Pending',
       'approved' => 'Approved',
       'rejected' => 'Rejected',
       'disabled' => 'Disabled'
    );


    // protected $with = 'rating';

    public function getLogoAttribute($logo)
    {
        return (isset($logo))?asset('storage/restaurant/'.$logo):null;
    }

    public function timings()
    {
        return $this->hasOne('App\Models\RestaurantTiming');
    }

    /**
     * Get all of the restaurant's categories.
     */
    public function categories()
    {
        return $this->morphMany(Category::class, 'categoryable');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
    
    public function productsLatest10()
    {
        return $this->products()->where('is_featured', 0)->whereIn('status', ['approved', 'enabled'])->latest()->take(10);
    }

    public function featuredProducts()
    {
        return $this->hasMany('App\Models\Product')->where('is_featured', 1)->where('status', 'approved');
    }

    public function featuredProductsLatest10()
    {
        return $this->featuredProducts()->latest()->take(10);
    }

    public function deals()
    {
        return $this->hasMany('App\Models\Deal');
    }

    public function dealsLatest10()
    {
        return $this->deals()->where('status', 1)->latest()->take(10);
    }

    public function rating()
    {
        return 0;
    }

    static function pending()
    {
        return self::where('status', 'pending');
    }

    public function scopeActive($query)
    {
        return $query->where('status', 'active')->orWhere('status', 'approved');
    }

    public function scopePopular($query, $perPage=null)
    {
        return (is_null($perPage))?$query->active()->latest():$query->active()->latest()->take($perPage);
    }


}
