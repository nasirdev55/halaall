<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'restaurant_id',
        'cart_id',
        'payment_type',
        'total',
        'commision_percent',
        'total_commision',
        'commision_status'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $with = [
        'orderDetail',
        'orderStatus'
    ];

    public function orderDetail()
    {
        return $this->hasOne('App\Models\OrderDetail');
    }

    public function orderStatus()
    {
        return $this->hasMany('App\Models\OrderStatus');
    }
}
