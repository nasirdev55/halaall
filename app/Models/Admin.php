<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'avatar',
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'created_at', 'updated_at'
    ];

    public function getAvatarAttribute($avatar)
    {
        return (isset($avatar))?asset('storage/admin/'.$avatar):null;
    }

    /**
     * Get all of the admin's category.
     */
    public function categories()
    {
        return $this->morphMany(Category::class, 'categoryable');
    }
}
