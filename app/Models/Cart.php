<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = [
        'user_id',
        'restaurant_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $with = [
        'products', 'deals'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function restaurant()
    {
        return $this->belongsTo('App\Models\Restaurant');
    }

    public function items()
    {
        return $this->hasMany('App\Models\CartItem');
    }

    /**
     * Get all of the product's cart.
     */
    public function products()
    {
        return $this->items()->where('item_type', 'product');
    }

    public function deals()
    {
        return $this->items()->where('item_type', 'deal');
    }
}
