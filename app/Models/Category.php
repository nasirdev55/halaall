<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Category extends Model
{
    protected $fillable = [
        'name',
        'image',
    ];

    protected $hidden = [
        'categoryable_id',
        'categoryable_type',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'status' => 'boolean'
    ];

    protected static function boot()
    {
        parent::boot();

        // static::addGlobalScope(new ActiveScope);
    }

    /**
     * Get all of the owning categoryable models.
     */
    public function categoryable()
    {
        return $this->morphTo();
    }

    public function getNameAttribute($name)
    {
        return ucfirst($name);
    }

    public function getImageAttribute($image)
    {
        return asset('storage/category/'.$image);
    }

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    static function categories($perPage=null)
    {
        return (is_null($perPage))?self::latest():self::latest()->take($perPage);
    }

    public function scopeLatestRecords($query, $perPage=null)
    {
        return (is_null($perPage))?$query->latest():$query->latest()->take($perPage);
    }

    public function products()
    {
        return $this->hasMany(Product::class,'category_id');
    }
}
