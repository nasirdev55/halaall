<?php
namespace App\Http\Traits;

use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

trait ImageUpload {

    public function uploadImage($request, $fileName, $dir, $disk='public')
    {
        $image_name = Carbon::now()->timestamp."".uniqid().".".'png';
        if (!Storage::disk($disk)->exists($dir)) {
            Storage::disk($disk)->makeDirectory($dir);
        }
        
        $image_path = Storage::disk('public')->putFileAs($dir, $request->file($fileName), $image_name);
        
        return $image_name;
    }

    public function uploadImageOld($request, $fileName, $dir, $disk='public')
    {
        $image_name = Carbon::now()->timestamp."".uniqid().".".'png';
        if (!Storage::disk($disk)->exists($dir)) {
            Storage::disk($disk)->makeDirectory($dir);
        }
        
        $note_img = Image::make($request->file($fileName))->stream();
        Storage::disk($disk)->put($dir.'/'.$image_name, $note_img);
        return $image_name;
    }

    public function uploadMultipleImages($request, $fileName, $dir, $disk='public')
    {
        if (!Storage::disk($disk)->exists($dir)) {
            Storage::disk($disk)->makeDirectory($dir);
        }

        $output = array();

        $images = $request->file($fileName);
        foreach($images as $image) {
            $image_name = Carbon::now()->timestamp."".uniqid().".".'png';
            // $note_img   = Image::make($image)->stream();
            // Storage::disk($disk)->put($dir.'/'.$image_name, $note_img);

            $image_path = Storage::disk('public')->putFileAs($dir, $image, $image_name);
            array_push($output, $image_name);
        }
        
        return $output;
    }

    public function removeImage($fileName,$dir, $disk='public')
    {
        // dd($dir.' '.$fileName.' '.$disk);
        Storage::disk($disk)->delete($dir.'/'.$fileName);
    }
}