<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Services\RestaurantService;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\RestaurantCreateRequest;
use App\Http\Requests\RestaurantUpdateRequest;

class RestaurantController extends Controller
{
//    private $restaurantService;

//    public function __construct(RestaurantService $restaurantService)
//    {
//        $this->restaurantService = $restaurantService;
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurants = Restaurant::where('status', '!=', 'pending')->orderBy('id')->paginate(10);
        return view('admin.restaurant.index', compact('restaurants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.restaurant.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RestaurantCreateRequest $request)
    {
        try {
            $request->validate([
                'name'     => 'required|string|unique:restaurants,name',
                'email'    => 'required|string|unique:restaurants,email',
                'phone'    => 'required|string|unique:restaurants,phone',
                'password' => 'required|string',
//            'status'   => 'required|boolean',
                'address'  => 'required|string',
                'delivery_time'    => 'nullable|numeric',
                'delivery_charges' => 'required_with:delivery_time',
                'start_time'       => 'nullable|string',
                'end_time'         => 'required_with:start_time',
//                'logo'             => 'nullable|image|mimes:jpeg,png,jpg'
            ]);

            if ($request->hasFile('logo')){

                $request->validate([
                    'logo' => 'mimes:jpeg,bmp,png'
                ]);

                $request->logo->store('admin/restaurant', 'public');
                $request['status'] = 'approved';

                $restau = new Restaurant([
                    'name'     => $request->get('name'),
                    'email'    => $request->get('email'),
                    'phone'    => $request->get('phone'),
                    'password' => $request->get('password'),
                    'address'  => $request->get('address'),
                    'delivery_time'    => $request->get('delivery_time'),
                    'delivery_charges' => $request->get('delivery_charges'),
                    'start_time'       => $request->get('start_time'),
                    'end_time'         => $request->get('end_time'),
                    'status'         => $request->get('status'),
                    'logo'         => $request->logo->hashName(),
                ]);
                $restau->save();
                Toastr::success('Restaurant added successfully!');
            }
        } catch (\Exception $e) {
            Toastr::error("There is an error! Please try again.".$e->getMessage());
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function show(Restaurant $restaurant)
    {
        // dd($restaurant);
        return view('admin.restaurant.show', compact('restaurant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function edit(Restaurant $restaurant)
    {
        return view('admin.restaurant.edit', compact('restaurant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function update(RestaurantUpdateRequest $request, Restaurant $restaurant)
    {
        try {
            $this->restaurantService->update($restaurant, $request);
            Toastr::success('Restaurant updated successfully!');
        } catch (\Exception $e) {
            Toastr::error($e->getMessage());
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Restaurant  $restaurant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restaurant $restaurant)
    {
        //
    }

    public function search(Request $request)
    {
        $key = explode(' ', $request['search']);
        $restaurants = Restaurant::where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->get();
        return response()->json([
            'view'=>view('admin.restaurant.partials._table',compact('restaurants'))->render()
        ]);
    }

    public function requests()
    {
        $restaurants = Restaurant::pending()->orderBy('id')->paginate(10);
        return view('admin.restaurant-request.index', compact('restaurants'));
    }

    public function requestSearch(Request $request)
    {
        $restaurants = $this->restaurantService->requestSearch($request);
        return response()->json([
            'view'=>view('admin.restaurant-request.partials._table',compact('restaurants'))->render()
        ]);
    }

    public function requestUpdateStatus(Request $request, Restaurant $restaurant, $status)
    {
        try {
            $this->restaurantService->requestUpdateStatus($restaurant, $status);
            Toastr::success('Restaurant status updated successfully!');
        } catch (\Exception $e) {
            Toastr::error("There is an error! Please try again.".$e->getMessage());
        }

        return back();
    }
}
