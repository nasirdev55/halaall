<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Services\ProductService;
use Brian2694\Toastr\Facades\Toastr;

class ProductController extends Controller
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->productService->index();
        return view('admin.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // $product->load('category');
        $product->load('restaurant');
        $categories = optional($product->restaurant)->categories;
        return view('admin.product.show', compact('product', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateStatus(Request $request, Product $product, $status)
    {
        try {
            $this->productService->updateStatus($product, $status);
            Toastr::success('Product status updated successfully!');
        } catch (\Exception $e) {
            Toastr::error("There is an error! Please try again.".$e->getMessage());
        }

        return back();
    }

    public function requests()
    {
        $products = $this->productService->requests();
        return view('admin.product-request.index', compact('products'));
    }

    public function search(Request $request)
    {
        $products = $this->productService->search($request);
        return response()->json([
            'view'=>view('admin.product.partials._table',compact('products'))->render()
        ]);
    }

    public function requestSearch(Request $request)
    {
        $products = $this->productService->requestSearch($request);
        return response()->json([
            'view'=>view('admin.product-request.partials._table',compact('products'))->render()
        ]);
    }

    public function requestUpdateStatus(Request $request, Product $product, $status)
    {
        // dd($status);
        try {
            $this->productService->requestUpdateStatus($product, $status);
            Toastr::success('Product status updated successfully!');
        } catch (\Exception $e) {
            Toastr::error("There is an error! Please try again.".$e->getMessage());
        }

        return back();
    }
}
