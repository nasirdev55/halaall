<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Services\AdminService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class SystemController extends Controller
{
    private $adminService;

    public function __construct(AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function restaurant_data()
    {
        $new_order = DB::table('orders')->where(['checked' => 0])->count();
        return response()->json([
            'success' => 1,
            'data' => ['new_order' => $new_order]
        ]);
    }

    public function settings()
    {
        $admin = auth()->guard('admin')->user();
        return view('admin.settings', compact('admin'));
    }

    public function profile(Request $request)
    {
        $request->validate([
            'first_name' => 'required',
            'last_name'  => 'required',
            'phone'      => 'required',
            'images'     => 'nullable|image|mimes:jpeg,png,jpg'
        ]);

        $admin = auth()->guard('admin')->user();

        try {
            $admin = $this->adminService->updateProfile($admin, $request);
            Toastr::success('Admin updated successfully!!');
        } catch (\Exception $e) {
            Toastr::error("There is an error! Please try again.".$e->getMessage());
        }

        return back();
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'password'     => 'required|same:confirm_password|min:6',
        ]);
       
        try {
            $admin = auth()->guard('admin')->user();
            if ($this->adminService->updatePassword($admin, $request)){
                Toastr::success('Admin password updated successfully!');
            }else{
                Toastr::error('Old Password does not match!');
            }
        } catch (\Exception $e) {
            Toastr::error($e->getMessage());
        }
        return back();
    }
}
