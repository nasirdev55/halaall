<?php

namespace App\Http\Controllers\Api\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DealService;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\Restaurant\Deal\DealCreateRequest;
use App\Http\Requests\Api\Restaurant\Deal\DealUpdateRequest;

class DealController extends Controller
{
    private $dealService;

    public function __construct(DealService $dealService)
    {
        $this->dealService = $dealService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealCreateRequest $request)
    {
        try {
            $deal = $this->dealService->create($request);
            $msg = trans('apiresponses.restaurant.deal.create');
            return Response::success($msg, $deal);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\DealCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(DealUpdateRequest $request)
    {
        try {
            $deal = $this->dealService->update($request);
            $msg = trans('apiresponses.restaurant.deal.update');
            return Response::success($msg, $deal);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
