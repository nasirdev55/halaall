<?php

namespace App\Http\Controllers\Api\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\RestaurantService;

class HomeController extends Controller
{
    private $service;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->service = $restaurantService;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $data = $this->service->dashboard();
        return Response::success($data);
    }
}
