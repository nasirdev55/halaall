<?php

namespace App\Http\Controllers\Api\Restaurant;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Services\RestaurantAccountService;
use App\Http\Requests\Api\Restaurant\AccountCreateRequest;
use App\Http\Requests\Api\Restaurant\AccountUpdateRequest;

class AccountController extends Controller
{

    private $accountService;

    public function __construct(RestaurantAccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\AccountCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountCreateRequest $request)
    {
        try {
            $restaurantAccount = $this->accountService->create($request);
            $msg = trans('apiresponses.restaurant.account.register');
            return Response::success($msg, $restaurantAccount);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\AccountUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AccountUpdateRequest $request)
    {
        // $restaurantAccountObj = RestaurantAccount::find($request->account_id);
        // if (RestaurantAccountService::accountExist($restaurantAccountObj, $request->restaurant_id)) {
            $restaurantAccountObj = $this->accountService->update($request);
            $msg = trans('apiresponses.restaurant.account.update');
            return Response::success($msg, $restaurantAccountObj);
        // }
        // return Response::error('Your provided information is not correct!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
