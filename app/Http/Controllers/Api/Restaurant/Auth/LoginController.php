<?php

namespace App\Http\Controllers\Api\Restaurant\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\Restaurant\Auth\LoginRequest;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $data = [
            'email'    => $request->email,
            'password' => $request->password
        ];

        if (auth()->guard('restaurant')->attempt($data)) {
            $restaurant = auth()->guard('restaurant')->user();
            // dd($restaurant);
            if ($restaurant->status) {
                $token = auth()->guard('restaurant')->user()->createToken('RestaurantAuth')->accessToken;
                $msg = trans('apiresponses.restaurant.login');
                return Response::successWithToken($token, $msg, $restaurant);
            }

            return Response::error(
                trans('apiresponses.restaurant.account.unapproved'), 403);
        }

        return Response::error(
                trans('apiresponses.restaurant.account.mismatch'), 401);
    }
}
