<?php

namespace App\Http\Controllers\Api\Restaurant\Auth;

use App\Http\Controllers\Controller;
use App\Services\RestaurantService;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\Restaurant\Auth\RegisterRequest;

class RegisterController extends Controller
{
    private $restaurantService;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
    }
    
    public function register(RegisterRequest $request)
    {
        try {
            $restaurant = $this->restaurantService->register($request);
            $msg = trans('apiresponses.restaurant.register');
            return Response::success($msg, $restaurant);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }
}
