<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\User\Auth\AccountVerifyRequest;

class VerificationController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function verify(AccountVerifyRequest $request)
    {
        try {
            $user = $this->userService->verify($request);
            if ($user['status']) {
                $msg = trans('apiresponses.user.account.verify');
                return Response::successWithToken($user['token'], $msg, $user['user']);
            }
            $msg = trans('apiresponses.user.account.verificationcodenotexist');
            return Response::error($msg);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    public function resendVerificationCode($value='')
    {
        // code...
    }
}
