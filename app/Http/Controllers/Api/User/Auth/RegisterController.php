<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\User\Auth\RegisterRequest;

class RegisterController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(RegisterRequest $request)
    {
        try {
            $user = $this->userService->register($request);
            $msg = trans('apiresponses.user.register');
            return Response::success($msg, $user);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }
}
