<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\User\Auth\ForgotPasswordRequest;

class ForgotPasswordController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function forgotPassword(ForgotPasswordRequest $request)
    {
        try {
            $isSent = $this->userService->forgotPassword($request);
            if ($isSent) {
                $msg = trans('apiresponses.user.forgotpasswordemail.sent');
                return Response::success($msg);
            }
            $msg = trans('apiresponses.user.forgotpasswordemail.notsent');
            return Response::error($msg);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    public function resendVerificationCode($value='')
    {
        // code...
    }
}
