<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\User\Auth\ResetPasswordRequest;

class ResetPasswordController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('auth:api');
        $this->userService = $userService;
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        try {
            $user = $this->userService->resetPassword($request);
            $token = $user->createToken('resetPassword')->accessToken;
            $msg = trans('apiresponses.user.resetpassword');
            return Response::successWithToken($token, $msg, $user);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    public function resendVerificationCode($value='')
    {
        // code...
    }
}
