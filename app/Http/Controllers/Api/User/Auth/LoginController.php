<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\User\Auth\LoginRequest;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        $data = [
            'email'    => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($data)) {
            $user = auth()->user();
            if ($user->is_verified) {
                $token = auth()->user()->createToken('userAuth')->accessToken;
                $msg = trans('apiresponses.user.login');
                return Response::successWithToken($token, $msg, $user);
            }

            return Response::error(
                trans('apiresponses.user.account.notverified'), 403);
        }

        return Response::error(
                trans('apiresponses.user.account.mismatch'), 401);
    }
}
