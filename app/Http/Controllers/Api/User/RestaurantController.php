<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Restaurant;
use App\Services\RestaurantService;
use App\Http\Requests\Api\User\RestaurantDetailRequest;

class RestaurantController extends Controller
{
    private $restaurantService;
    private $responseConstants;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
        $this->responseConstants = config('constants');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(RestaurantDetailRequest $request)
    {
        $res = $this->restaurantService->detailsForApi($request->restaurant_id);
        return response()->success('Restaurant Details', $res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function products(Request $request)
    {
        $restaurant = Restaurant::find($request->restaurant_id);
        $res = $this->restaurantService->products($restaurant, $request);
        return response()->success('Product List', $res);
    }

    public function featuredProduct(Request $request)
    {
        $restaurant = Restaurant::find($request->restaurant_id);
        $res = $this->restaurantService->featuredProduct($restaurant, $request);
        return response()->success('Featured Product List', $res);
    }

    public function deals(Request $request)
    {
        $restaurant = Restaurant::find($request->restaurant_id);
        $res = $this->restaurantService->deals($restaurant, $request);
        return response()->success('Deal List', $res);
    }
}
