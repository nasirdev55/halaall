<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;
use App\Services\CartService;
use Illuminate\Support\Facades\Response;
use App\Http\Requests\Api\User\Cart\CartCreateRequest;
use App\Http\Requests\Api\User\Cart\CartUpdateItemRequest;
use App\Http\Requests\Api\User\Cart\CartRemoveItemRequest;

class CartController extends Controller
{

    private $cartService;
    private $responseConstants;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
        $this->responseConstants = config('constants');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CartCreateRequest $request)
    {
        try {
            $user = auth('api')->user();
            $cart = $this->cartService->addItems($user, $request);
            $msg = trans('apiresponses.cart.items.add');
            return Response::success($msg, $cart);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function cart(Request $request)
    {
        // $user = auth('api')->user();
        // return response()->json($user->cart);
        try {
            $user = auth('api')->user();
            $cart = $this->cartService->cartDetails($user, $request);
            $msg = trans('apiresponses.cart.list');
            return Response::success($msg, $cart);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function edit(Cart $cart)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cart  $cart
     * @return \Illuminate\Http\Response
     */
    public function update(CartUpdateItemRequest $request)
    {
        try {
            $this->cartService->updateItem($request);
            $msg = trans('apiresponses.cart.items.update');
            return Response::success($msg);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }

    public function remove(CartRemoveItemRequest $request)
    {
        try {
            $this->cartService->removeItem($request);
            $msg = trans('apiresponses.cart.items.remove');
            return Response::success($msg);
        } catch (\Exception $e) {
            return Response::error($e->getMessage());
        }
    }
}
