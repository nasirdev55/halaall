<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\User;

class DashboardController extends Controller
{
    private $userService;
    private $responseConstants;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
        $this->responseConstants = config('constants');
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $res = $this->userService->dashboard();
        return response()->success('Dashboard Data', $res);
    }
}
