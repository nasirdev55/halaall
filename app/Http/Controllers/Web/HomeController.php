<?php

namespace App\Http\Controllers\Web;

use App\Models\Category;
use App\Http\Traits\ImageUpload;
use Illuminate\Http\Request;
use App\Services\UserService;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryUpdateRequest;

class HomeController extends Controller
{
    private $userService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
       $data = $this->userService->dashboard();
       return view('frontend.home', compact('data'));
    }
}
