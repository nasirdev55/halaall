<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\SearchService;

class SearchController extends Controller
{
    private $searchService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SearchService $searchService)
    {
        // $this->middleware('auth');
        $this->searchService = $searchService;
    }

    public function search(Request $request)
    {
        $data = $this->searchService->search($request);
        return view('frontend.search', compact('data'));
    }

    public function restaurantSearch(Request $request)
    {
        $data = $this->searchService->restaurantSearch($request);
        $restaurant        = $data['restaurant'];
        $featured_products = $data['featured_products'];

        return view('frontend.restaurant-details', compact('restaurant', 'featured_products'));
    }
}
