<?php

namespace App\Http\Controllers\Web;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\UserService;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
use App\Http\Requests\RestaurantDetailRequest;

class RestaurantController extends Controller
{
    private $userService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        // $this->middleware('auth');
        $this->userService = $userService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(RestaurantDetailRequest $request)
    {
        $data = $this->userService->restaurantDetail($request);
        $restaurant        = $data['restaurant'];
        $featured_products = $data['featured_products'];
// dd($restaurant);
        return view('frontend.restaurant-details', compact('restaurant', 'featured_products'));
    }
}
