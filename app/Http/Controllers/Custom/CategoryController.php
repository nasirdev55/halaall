<?php

namespace App\Http\Controllers\Custom;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function categoryDetail(Request $request,CategoryService $categoryService,$detail)
    {
//        dd($detail);
        return $categoryService->categoryDetail($request,$detail);
    }
}
