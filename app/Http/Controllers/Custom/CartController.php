<?php

namespace App\Http\Controllers\Custom;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function cart(Request $request,Product $product_id)
    {
        $data = $request->validate([
            'product_id' => '',
            'restaurant_id' => '',
            'category_id' => '',
            'price' => '',
            'total_price' => '',
            'quantity' => '',
            'email' => '',
            'session_id' => '',
        ]);
        dd($request->all());

    }
}
