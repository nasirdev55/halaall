<?php

namespace App\Http\Controllers\Custom;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function productDetail(Request $request,ProductService $productService,$detail)
    {
        return $productService->productDetail($request,$detail);
    }
}
