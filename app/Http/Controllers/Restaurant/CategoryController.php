<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Category;
use App\Http\Traits\ImageUpload;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Restaurant\CategoryController;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryUpdateRequest;

class CategoryController extends Controller
{
    use ImageUpload;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd($this->restaurant);
        $restaurant = auth()->guard('restaurant')->user();
        // dd($restaurant);
        $categories = $restaurant->categories()->latest()->paginate(10);
        return view('restaurant.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreateRequest $request)
    {
        $req = $request->validated();
        $restaurant = auth()->guard('restaurant')->user();

        try {
            $category = new Category;
            $category->name = $req['name'];
            if ($request->hasFile('image')) {
                $category->image = $this->uploadImage($request, 'image', 'category');
            }

            $restaurant->categories()->save($category);
            Toastr::success('Category added successfully!');
            return back();
        } catch (\Exception $e) {
            Toastr::exception($e->getMessage());
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function categoryDetail(Request $request,Category $category)
    {
        dd($category,$request->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('restaurant.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        $req = $request->validated();
        $category->name = $req['name'];
        if ($request->hasFile('image')) {
            $this->removeImage($category->image, 'category');
            $category->image = $this->uploadImage($request, 'image', 'category');
        }

        $category->save();
        Toastr::success('Category updated successfully!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
    }

    public function updateStatus(Request $request, Category $category)
    {
        $category->status = $request->status;
        $category->save();
        Toastr::success('Category status updated!');
        return back();
    }


}
