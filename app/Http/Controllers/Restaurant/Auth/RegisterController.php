<?php

namespace App\Http\Controllers\Restaurant\Auth;

use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Services\RestaurantService;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Requests\RestaurantCreateRequest;

class RegisterController extends Controller
{
    private $restaurantService;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
    }

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RestaurantCreateRequest $request)
    {
        try {
            $request['status'] = 'pending';
            $restaurant = $this->restaurantService->register($request);
            Toastr::success('Restaurant added successfully!');
        } catch (\Exception $e) {
            Toastr::error("There is an error! Please try again.".$e->getMessage());
        }

        return back();
    }
}
