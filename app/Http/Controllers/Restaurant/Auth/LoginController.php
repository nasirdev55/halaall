<?php

namespace App\Http\Controllers\Restaurant\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:restaurant', ['except' => ['logout']]);
    }

    public function login()
    {
        return view('restaurant.auth.login');
    }

    public function submit(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (auth('restaurant')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            return redirect()->route('restaurant.dashboard');
        }

        return redirect()->back()->withInput($request->only('email', 'remember'))
            ->withErrors(['Credentials does not match.']);
    }

    public function logout(Request $request)
    {
        auth()->guard('restaurant')->logout();
        return redirect()->route('restaurant.auth.login');
    }
}
