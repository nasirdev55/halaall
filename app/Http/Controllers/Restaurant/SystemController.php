<?php

namespace App\Http\Controllers\Restaurant;

use App\Http\Controllers\Controller;
use App\Model\Admin;
use Illuminate\Http\Request;
use App\Services\RestaurantService;
use Brian2694\Toastr\Facades\Toastr;

class SystemController extends Controller
{
    private $restaurantService;

    public function __construct(RestaurantService $restaurantService)
    {
        $this->restaurantService = $restaurantService;
    }

    public function dashboard()
    {
        return view('restaurant.dashboard');
    }

    public function restaurant_data()
    {
        $new_order = DB::table('orders')->where(['checked' => 0])->count();
        return response()->json([
            'success' => 1,
            'data' => ['new_order' => $new_order]
        ]);
    }

    public function settings()
    {
        $restaurant = auth()->guard('restaurant')->user();
        return view('restaurant.settings', compact('restaurant'));
    }

    public function profile(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'phone' => 'required',
        ]);

        $restaurant = auth()->guard('restaurant')->user();

        try {
            $restaurant = $this->restaurantService->updateProfile($restaurant, $request);
            Toastr::success('Restaurant profile updated successfully!!');
        } catch (\Exception $e) {
            Toastr::error("There is an error! Please try again.".$e->getMessage());
        }

        return back();
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'password'     => 'required|same:confirm_password|min:6',
        ]);
       
        try {
            $restaurant = auth()->guard('restaurant')->user();
            if ($this->restaurantService->updatePassword($restaurant, $request)){
                Toastr::success('Restaurant password updated successfully!');
            }else{
                Toastr::error('Old Password does not match!');
            }
        } catch (\Exception $e) {
            Toastr::error($e->getMessage());
        }
        return back();
    }
}
