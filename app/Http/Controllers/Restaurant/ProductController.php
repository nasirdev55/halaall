<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Product;
use App\Models\ProductImage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Traits\ImageUpload;
use App\Services\ProductService;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;

class ProductController extends Controller
{
    use ImageUpload;

    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurant = auth()->guard('restaurant')->user();
        $products = $restaurant->products()->latest()->paginate(10);
        return view('restaurant.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restaurant = auth()->guard('restaurant')->user();
        $categories = $restaurant->categories()->orderBy('name')->get();
        return view('restaurant.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCreateRequest $request)
    {

        try {
            $product = $this->productService->create($request);
            Toastr::success('Product added successfully!');
            return back();
        } catch (\Exception $e) {
            Toastr::success($e->getMessage());
            return back();
        }

        $restaurantId = auth()->guard('restaurant')->id();

        $req = $request->validated();
        $product = new Product;
        $product->name  = $req['name'];
        $product->description = $req['description'];
        $product->price = $req['price'];
        $product->category_id   = $req['category_id'];
        $product->restaurant_id = $restaurantId;
        $product->is_featured   = $req['featured'];
        $product->is_available  = $req['available'];
        $product->save();

        if ($request->hasFile('images')) {
            $images = $this->uploadMultipleImages($request, 'images', 'product');
            $productImages = new ProductImage;
            foreach ($images as $key => $image) {
                $productImages->image = $image;
            }
            $product->images()->save($productImages);
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $restaurant = auth()->guard('restaurant')->user();
        $categories = $restaurant->categories()->orderBy('name')->get();
        return view('restaurant.product.show', compact('categories', 'product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // dd($product);
        $restaurant = auth()->guard('restaurant')->user();
        $categories = $restaurant->categories()->orderBy('name')->get();
        return view('restaurant.product.edit', compact('categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        $request['product_id'] = $product->id;
        try {
            $product = $this->productService->update($request);
            Toastr::success('Product added successfully!');
            return back();
        } catch (\Exception $e) {
            Toastr::error($e->getMessage());
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
