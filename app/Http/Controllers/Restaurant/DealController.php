<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Deal;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DealService;
use App\Http\Traits\ImageUpload;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Requests\DealCreateRequest;
use App\Http\Requests\DealUpdateRequest;

class DealController extends Controller
{
    use ImageUpload;

    private $dealService;

    public function __construct(DealService $dealService)
    {
        $this->dealService = $dealService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurant = auth()->guard('restaurant')->user();
        $deals = $restaurant->deals()->latest()->paginate(10);
        return view('restaurant.deal.index', compact('deals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('restaurant.deal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DealCreateRequest $request)
    {
        try {
            $deal = $this->dealService->create($request);
            Toastr::success('Deal added successfully!');
            return back();
        } catch (\Exception $e) {
            Toastr::error($e->getMessage());
            return back();
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function show(Deal $deal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function edit(Deal $deal)
    {
        return view('restaurant.deal.edit', compact('deal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function update(DealUpdateRequest $request, Deal $deal)
    {
        try {
            $request['deal_id'] = $deal->id;
            $deal = $this->dealService->update($request);
            Toastr::success('Deal updated successfully!');
            return back();
        } catch (\Exception $e) {
            Toastr::error($e->getMessage());
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Deal  $deal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Deal $deal)
    {
        //
    }
}
