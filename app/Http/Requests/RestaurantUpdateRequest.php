<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RestaurantUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->restaurant);
        return [
            'name'     => 'required|string|unique:restaurants,name,'.$this->restaurant->id.',id',
            'email'    => 'required|string|unique:restaurants,email,'.$this->restaurant->id.',id',
            'phone'    => 'required|string|unique:restaurants,phone,'.$this->restaurant->id.',id',
            // 'password' => 'required|string',
            // 'status'   => 'required|boolean',
            'address'  => 'required|string',
            'delivery_time'    => 'nullable|numeric',
            'delivery_charges' => 'required_with:delivery_time',
            'start_time'       => 'nullable|string',
            'end_time'         => 'required_with:start_time',
            'logo'             => 'nullable|image|mimes:jpeg,png,jpg'
        ];
    }
}
