<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name'        => 'required|string|unique:products,name,null,'.$this->product->id.',id',
            'name'        => 'required|string',
            'description' => 'nullable|string',
            'price'       => 'required|string',
            // 'category_id' => 'required|numeric|exists:categories,id,'.$this->product->restaurant_id,
            'category_id' => 'required|numeric',
            'featured'    => 'nullable|boolean',
            'available'   => 'nullable|boolean',
            'images.*'    => 'nullable|image|mimes:jpeg,png,jpg'
        ];
    }
}
