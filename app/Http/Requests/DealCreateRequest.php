<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DealCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string|unique:deals,name,null,restaurant_id'.$this->restaurant_id,
            'description'   => 'nullable|string',
            'price'         => 'required|string',
            'delivery_time' => 'required|numeric|min:5|max:60',
            'images.*'      => 'nullable|image|mimes:jpeg,png,jpg'
        ];
    }
}
