<?php

namespace App\Http\Requests\Api\User\Order;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class OrderCheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'restaurant_id'     => 'required|integer|exists:restaurants,id',
            'cart_id'           => 'required|integer|exists:carts,id',
            'payment_type'      => 'required|string',
            'commision_percent' => 'required|numeric',
            'total_commision'   => 'required|numeric',
            'delivery_charges'  => 'required|numeric',
            'tax_percent'       => 'nullable|numeric',
            'tax'               => 'nullable|numeric',
            'sub_total'         => 'required|numeric',
            'total'             => 'required|numeric',
            'payment_id'        => 'nullable|string',
            'payment_status'    => 'required|string',

        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->validationError($validator->errors()->first()));
    }
}
