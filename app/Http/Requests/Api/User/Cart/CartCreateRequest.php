<?php

namespace App\Http\Requests\Api\User\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CartCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "cart_id"       => "nullable|numeric|exists:carts,id",
            "restaurant_id" => "required|numeric|exists:restaurants,id",
            'items'         => 'required|array',
            "items.*.unit_price" => 'required|numeric',
            "items.*.quantity"   => 'required|numeric',
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->validationError($validator->errors()->first()));
    }
}
