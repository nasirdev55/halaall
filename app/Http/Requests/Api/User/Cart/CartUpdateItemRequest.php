<?php

namespace App\Http\Requests\Api\User\Cart;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class CartUpdateItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "cart_id"    => "required|numeric|exists:carts,id",
            "item_type"  => "required|string|".Rule::in(['product', 'deal']),
            'item_id'    => "required|numeric|exists:".$this->item_type."s,id",
            "quantity"   => 'required|numeric',
            "unit_price" => 'required|numeric',
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->validationError($validator->errors()->first()));
    }
}
