<?php

namespace App\Http\Requests\Api\Restaurant\Deal;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class DealCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string|unique:deals,name,null,null,restaurant_id,'.auth()->id(),
            'description'   => 'nullable|string',
            'price'         => 'required|string',
            'delivery_time' => 'required|numeric|min:5|max:60',
            'image'         => 'required|image|mimes:jpeg,png,jpg'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->validationError($validator->errors()->first()));
    }
}
