<?php

namespace App\Http\Requests\Api\Restaurant\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'   => 'required|integer|exists:categories,id',
            'name'          => 'required|string',
            'description'   => 'nullable|string',
            'price'         => 'required|string',
            // 'delivery_time' => 'required|string',
            'images.*'      => 'nullable|image|mimes:jpeg,png,jpg'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->validationError($validator->errors()->first()));
    }
}
