<?php

namespace App\Http\Requests\Api\Restaurant\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|string|unique:restaurants,name',
            'email'    => 'required|string|unique:restaurants,email',
            'phone'    => 'required|string|unique:restaurants,phone',
            'password' => 'required|string',
            'status'   => 'nullable|boolean',
            'address'  => 'required|string',
            'delivery_time'    => 'nullable|numeric',
            'delivery_charges' => 'required_with:delivery_time',
            'start_time'       => 'nullable|string',
            'end_time'         => 'required_with:start_time',
            'logo'             => 'nullable|image|mimes:jpeg,png,jpg'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->validationError($validator->errors()->first()));
    }
}
