<?php

namespace App\Http\Middleware;

use Closure;

class CheckRestaurantActiveOrApproved
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $status  = true;
        $message = '';
        switch (auth()->guard('restaurant')->user()->status) {
            case 'pending':
                $status  = false;
                $message = 'Your request has not been approved by administration. Please wait for 2-3 business working days. You can not make any entry!. If you have any query please feel free to contact us at info@justhalaall.com';
                break;
            case 'active':
            case 'approved':
                $status = true;
                break;
            case 'rejected':
                $status  = false;
                $message = 'Your request has been rejected by administration. You can not make any entry!. If you have any query please feel free to contact us at info@justhalaall.com';
                break;
            case 'disabled':
                $status = false;
                $message = 'Your request has been disabled by administration. You can not make any entry!. If you have any query please feel free to contact us at info@justhalaall.com';
                break;
            default:
                $status = false;
                break;
        }

        $request->session()->put('profileStatus', $status);
        $request->session()->put('profileStatusMessage', $message);
        return $next($request);
    }
}
