<?php
  
if (!function_exists('getCount')) {
    /**
     * Generate an currency path for the application.
     *
     * @param  mix
     * @return integer
     */
    function getCount($arg)
    {
        // dd(gettype($arg));
        switch (gettype($arg)) {
            case 'object':
            case 'array':
                return count($arg);
                break;

            default:
                return 0;
                break;
        }
    }
}

if (!function_exists('generateOtp')) {
    /**
     * Generate user verfication code.
     *
     * @param  mix
     * @return integer
     */
    function generateOtp()
    {
        return random_int(1111, 9999);
    }
}

// if (!function_exists('paginate')) {
//     /**
//      * Paginate the collection
//      *
//      * @param  mix
//      * @return integer
//      */
//     function paginate($items, $perPage = , $page = null, $options = [])
//     {
//         $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
//         $items = $items instanceof Collection ? $items : Collection::make($items);
//         return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
//     }
// }

if (!function_exists('paginatedData')) {
    /**
     * PaginatedData the collection
     *
     * @param  mix
     * @return integer
     */
    function paginatedData($modelName='products', $paginator)
    {
        $output = array(
            'current_page' => $paginator->currentPage(),
            'total'        => $paginator->total(),
            'last_page'    => $paginator->lastPage(),
            $modelName     => array()
        );

        foreach ($paginator as $key => $item) {
            $output[$modelName][] = $item;
        }

        return $output;
    }
}

