<?php

namespace App\Services;

use App\Models\Category;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Request;

class WebService
{
    public function home()
    {
        $data = array();
        $data['categories'] = Category::categories()->get(['id','name', 'image']);
        $data['popular_restaurants'] = Restaurant::popular()->get(['id','name', 'logo']);
        return $data;
    }
}
