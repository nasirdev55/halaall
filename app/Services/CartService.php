<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\CartItem;
use App\Http\Traits\ImageUpload;
use Illuminate\Support\Facades\Request;

class CartService
{
    // Check request restaurantId and old Cart restaurant id is same
    // else remove all old Restaurant items from cart, Then add new restaurant items to cart
    public function addItems($user, $request)
    {
        if ($request['cart_id']) {
            $cart = Cart::where('id', $request['cart_id'])->where('user_id', $user->id)->where('status', 'pending')->first();
            if ($this->_restaurantIsSame($request['restaurant_id'], $cart->restaurant_id)) {
                return $this->_addRestaurantItems($cart, $request['items']);
            }else{
                $this->_removeOldRestaurantItems($cart);
            }
        }else{
            // Create Cart first
            $cart = Cart::create([
                'user_id'       => $user->id,
                'restaurant_id' => $request['restaurant_id']
            ]);

            return $this->_addRestaurantItems($cart, $request['items']);
        }
    }

    public function updateItem($request)
    {
        return CartItem::where('cart_id', $request['cart_id'])->where('item_id', $request['item_id'])->where('item_type', $request['item_type'])->update([
                'quantity'   => $request['quantity'],
                'unit_price' => $request['unit_price']
            ]);
    }

    public function removeItem($request)
    {
        return CartItem::where('cart_id', $request['cart_id'])->where('item_id', $request['item_id'])->where('item_type', $request['item_type'])->delete();
    }

    public function cartDetails($user, $request)
    {
        $result = $user->cart()->paginate(10);
        return paginatedData('cart', $result);
    }

    public function _restaurantIsSame($oldId, $newId)
    {
        return ($oldId == $newId)?true:false;
    }

    public function _addRestaurantItems($cart, $items)
    {
        $newItems = array();
        foreach ($items as $key => $item) {
            $newItem = new CartItem;
            $newItem->item_id    = $item['item_id'];
            $newItem->item_type  = $item['item_type'];
            $newItem->unit_price = $item['unit_price'];
            $newItem->quantity   = $item['quantity'];

            array_push($newItems, $newItem);
        }

        $cart->items()->saveMany($newItems);
    }

    public function _removeOldRestaurantItems($cart)
    {
        return $cart->items()->delete();
    }
}
