<?php

namespace App\Services;

use App\Models\Deal;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Request;
use App\Services\ProductService;
use App\Services\RestaurantService;

class SearchService
{
    public function search($request)
    {
        $output = array(
            'products' => [],
            'restaurants' => []
        );

        if (empty($request['term'])) {
            return $output;
        }

        $output['products'] = (new ProductService)->filter($request);
        $output['restaurants'] = (new RestaurantService)->filter($request);

        return $output;
    }

    public function restaurantSearch($request)
    {
        $restaurant = Restaurant::where('name', $request->name)->first();
        if (!$restaurant) {
            abort(404);
        }

        $output = array();

        $restaurant = Restaurant::with(['products' => function($product) use ($request) {
            return $product->where('name', 'like', '%'.$request->term.'%')->where('is_featured', 0)->whereIn('status', ['approved', 'enabled']);
        }, 'deals' => function($deal) use ($request){
            return $deal->where('name', 'like', '%'.$request->term.'%');
        }])->where('name', 'like', '%'.$request->name.'%')->first();

        $output['restaurant'] = $restaurant;
        $output['featured_products'] = $restaurant->featuredProducts()->where('name', 'like', '%'.$request->name.'%')->get();

        return $output;
    }
}
