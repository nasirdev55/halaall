<?php

namespace App\Services;

use App\User;
use App\Models\Category;
use App\Models\Restaurant;
use App\Http\Traits\ImageUpload;
use App\Mail\UserVerificationMail;
use App\Mail\ForgotPasswordMail;

class UserService
{
    use ImageUpload;

    private $responseConstants;

    public function __construct()
    {
        $this->responseConstants = config('constants');
    }

    public function register($request)
    {
        $user = User::create([
            'name'         => $request->name,
            'email'        => $request->email,
            'phone'        => $request->phone,
            'password'     => \Hash::make($request->password),
            'address'      => $request->address,
            'verification_code' => generateOtp(),
            'device_token' => $request->device_token,
        ]);

        if ($request->hasFile('avatar')) {
            $user->avatar = $this->uploadImage($request, 'avatar', 'user');
            $user->save();
        }

        // Send verification email
        \Mail::to($user->email)->send(new UserVerificationMail($user));

        return $user;
    }

    public function verify($request)
    {
        $user = User::where('verification_code', $request->verification_code)->latest()->first();
        $output = array(
            'status' => false,
            'user'   => collect(),
            'token'  => '',
        );
        if ($user) {

            $user->is_verified = 1;
            $user->save();

            $output['status']  = true;
            $output['user']    = $user;
            $output['token']   = $user->createToken('userAuth')->accessToken;
        }

        return $output;
    }

    public function forgotPassword($request)
    {
        $user = User::where('email', $request->email)->first();
        $user->verification_code = generateOtp();
        $user->is_verified = 0;
        $user->save();

        \Mail::to($user->email)->send(new ForgotPasswordMail($user));

        return true;
    }

    public function resetPassword($request)
    {
        $user = auth()->user();
        $user->password = \Hash::make($request->password);
        $user->save();
        return $user;
    }

    public function dashboard()
    {
        $data = array();
        $data['categories'] = Category::active()->latestRecords($this->responseConstants['dashboard']['categories'])->get(['id','name', 'image']);
        $data['popular_restaurants'] = Restaurant::popular($this->responseConstants['dashboard']['popular']['restaurants'])->get(['id','name', 'logo', 'delivery_time']);
        return $data;
    }

    public function restaurantDetail($request)
    {
        $output = array();

        $restaurant = Restaurant::with(['products' => function($product) {
            return $product->where('is_featured', 0)->whereIn('status', ['approved', 'enabled']);
        }, 'deals'])->where('name', 'like', '%'.$request->name.'%')->first();

        $output['restaurant'] = $restaurant;
        $output['featured_products'] = $restaurant->featuredProducts()->get();

        return $output;
    }
}
