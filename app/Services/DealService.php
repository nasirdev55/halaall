<?php

namespace App\Services;

use App\Models\Deal;
use App\Http\Traits\ImageUpload;
use Illuminate\Support\Facades\Request;

class DealService
{
    use ImageUpload;

    public function create($request)
    {       
        $deal = Deal::create([
            'restaurant_id' => (Request::wantsJson())?auth()->id():auth()->guard('restaurant')->id(),
            'name'          => $request->name,
            'price'         => $request->price,
            'description'   => $request->description,
            'delivery_time' => $request->delivery_time
        ]);

        if ($request->hasFile('image')) {
            $deal->image = $this->uploadImage($request, 'image', 'deal');
            $deal->save();
        }

        return $deal->load('restaurant');
    }

    public function update($request)
    {
        $restaurantId = (Request::wantsJson())?auth()->id():auth()->guard('restaurant')->id();
        // dd($request->deal_id);
        $deal = Deal::where('id',$request->deal_id)->where('restaurant_id', $restaurantId)->first();

        $deal->name          = $request->name;
        $deal->price         = $request->price;
        $deal->description 	 = $request->description;
        $deal->delivery_time = $request->delivery_time;
        $deal->save();

        if ($request->hasFile('image')) {
            $deal->image = $this->uploadImage($request, 'image', 'deal');
            $deal->save();
        }

        return $deal->load('restaurant');
    }
}
