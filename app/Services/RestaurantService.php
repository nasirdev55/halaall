<?php

namespace App\Services;

use Exception;
use App\Models\Restaurant;
use App\Models\Product;
use App\Models\Deal;
use App\Models\RestaurantTiming;
use App\Http\Traits\ImageUpload;

class RestaurantService
{
    use ImageUpload;

    public function register($request)
    {
        $restaurant = Restaurant::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'phone'     => $request->phone,
            'password'  => \Hash::make($request->password),
            'latitude'  => 123456,
            'longitude' => 7654,
            'address'   => $request->address,
            'status'    => isset($request->status)?$request->status:'pending',
            'delivery_time'    => $request->delivery_time,
            'delivery_charges' => $request->delivery_charges
        ]);

        if ($request->hasFile('logo')) {
            $restaurant->logo = $this->uploadImage($request, 'logo', 'restaurant');
            $restaurant->save();
        }

        if ($request->has('start_time')) {
            $timings = new RestaurantTiming;
            $timings->start_time = $request->start_time;
            $timings->end_time   = $request->end_time;

            $restaurant->timings()->save($timings);
        }

        return $restaurant;
    }

    public function update($restaurant, $request)
    {
        $restaurant->name      = $request->name;
        $restaurant->email     = $request->email;
        $restaurant->phone     = $request->phone;
        $restaurant->password  = \Hash::make($request->password);
        $restaurant->latitude  = 123456;
        $restaurant->longitude = 7654;
        $restaurant->address   = $request->address;
        $restaurant->status    = isset($request->status)?$request->status:'pending';
        $restaurant->delivery_time    = $request->delivery_time;
        $restaurant->delivery_charges = $request->delivery_charges;

        if ($request->hasFile('logo')) {
            $this->removeImage($restaurant->logo, 'restaurant');
            $restaurant->logo = $this->uploadImage($request, 'logo', 'restaurant');
        }

        $restaurant->save();

        if ($request->has('start_time')) {
            $timings = RestaurantTiming::updateOrCreate([
                'restaurant_id' => $restaurant->id
            ],[
                'start_time' => $request->start_time,
                'end_time'   => $request->end_time,
            ]);
        }

        return $restaurant;
    }

    public function updateProfile($restaurant, $request)
    {
        $restaurant->name  = $request->name;
        $restaurant->phone = $request->phone;

        if ($request->hasFile('image')) {
            $this->removeImage($restaurant->logo, 'restaurant');
            $restaurant->logo = $this->uploadImage($request, 'image', 'restaurant');
        }

        return $restaurant->save();
    }

    public function updatePassword($restaurant, $request)
    {
        if (\Hash::check($request->old_password, $restaurant->password)) {
            $restaurant->password = \Hash::make($request->password);
            $restaurant->save();
            return true;
        }

        return false;
    }

    public function requestUpdateStatus($restaurant, $status)
    {
        if (array_key_exists($status, Restaurant::$statuses)) {
            $restaurant->status = $status;
            $restaurant->save();
            return true;
        }
        throw new Exception(" Invalid status passed", 1);
    }

    public function filter($request)
    {
        $key = explode(' ', $request['term']);
        return Restaurant::active()->where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->orderBy('id')->paginate(10);
    }

    public function requestSearch($request)
    {
        $key = explode(' ', $request['search']);
        return Restaurant::pending()->where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->orderBy('id')->paginate(10);
    }

    public function dashboard()
    {
        $restaurant = auth()->guard('restaurant')->user();
        return $restaurant->load('orders', 'activeDeals');
    }

    public function detailsForApi($restaurantId)
    {
        $output = array();

        $restaurant = Restaurant::with('productsLatest10', 'featuredProductsLatest10', 'dealsLatest10')->where('id', $restaurantId)->first();

        $output['restaurant'] = Restaurant::find($restaurantId);
        $output['products']   = $restaurant->productsLatest10;
        $output['featured_products'] = $restaurant->featuredProductsLatest10;
        $output['deals'] = $restaurant->dealsLatest10;

        return $output;
    }

    public function products($restaurant, $request)
    {
        $result = Product::where('restaurant_id', $restaurant->id)->paginate(10);

        return paginatedData('products', $result);
    }

    public function featuredProduct($restaurant, $request)
    {
        $result = Product::featuredProducts()->where('restaurant_id', $restaurant->id)->paginate(10);

        return paginatedData('featured_products', $result);
    }

    public function deals($restaurant, $request)
    {
        $result = Deal::active()->where('restaurant_id', $restaurant->id)->paginate(10);

        return paginatedData('deals', $result);
    }
}
