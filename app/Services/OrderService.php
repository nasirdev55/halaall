<?php

namespace App\Services;

use App\Models\Deal;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderStatus;
use App\Http\Traits\ImageUpload;
use Illuminate\Support\Facades\Request;

class OrderService
{

    public function checkout($user, $request)
    {      
        $order = Order::create([
            'user_id' => $user->id,
            'restaurant_id' => $request->restaurant_id,
            'cart_id' => $request->cart_id,
            'payment_type' => $request->payment_type,
            'total' => $request->total,
            'commision_percent' => $request->commision_percent,
            'total_commision' => $request->total_commision,
        ]);

        $orderDetail = OrderDetail::updateOrCreate([
            'order_id' => $order->id
        ],[
            'delivery_charges' => $request->delivery_charges,
            'tax_percent' => $request->tax_percent,
            'tax' => $request->tax,
            'commision_percent' => $request->commision_percent,
            'total_commision' => $request->total_commision,
            'sub_total' => $request->sub_total,
            'total' => $request->total,
            'payment_id' => $request->payment_id
        ]);

        $orderStatus = OrderStatus::create([
            'order_id' => $order->id,
            'status'   => 'pending'
        ]);

        return $order->load('orderDetail', 'orderStatus');
    }

    public function update($request)
    {
        $restaurantId = (Request::wantsJson())?auth()->id():auth()->guard('restaurant')->id();
        // dd($request->deal_id);
        $deal = Deal::where('id',$request->deal_id)->where('restaurant_id', $restaurantId)->first();

        $deal->name          = $request->name;
        $deal->price         = $request->price;
        $deal->description 	 = $request->description;
        $deal->delivery_time = $request->delivery_time;
        $deal->save();

        if ($request->hasFile('image')) {
            $deal->image = $this->uploadImage($request, 'image', 'deal');
            $deal->save();
        }

        return $deal->load('restaurant');
    }
}
