<?php

namespace App\Services;

use App\Models\RestaurantAccount;

class RestaurantAccountService
{

    public function create($request)
    {
        return RestaurantAccount::updateOrCreate([
            'restaurant_id' => $request->restaurant_id
        ],[
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'card_number'   => $request->card_number,
            'expiry'        => $request->expiry
        ]);
    }

    public static function accountExist($restaurantAccountObj, $restaurantId)
    {
        return ($restaurantAccountObj->restaurant_id === $restaurantId);
    }

    public function update($request)
    {
        $restaurantAccountObj = RestaurantAccount::where('restaurant_id',$request->restaurant_id)->first();
        $restaurantAccountObj->first_name  = $request->first_name;
        $restaurantAccountObj->last_name   = $request->last_name;
        $restaurantAccountObj->card_number = $request->card_number;
        $restaurantAccountObj->expiry      = $request->expiry;
        $restaurantAccountObj->save();

        return $restaurantAccountObj;
    }
}
