<?php

namespace App\Services;

use App\Models\Admin;
use App\Http\Traits\ImageUpload;
use Illuminate\Support\Facades\Request;

class AdminService
{
    use ImageUpload;

    public function updateProfile($admin, $request)
    {
        $admin->first_name = $request->first_name;
        $admin->last_name  = $request->last_name;
        $admin->phone      = $request->phone;
        if ($request->hasFile('image')) {
            $this->removeImage($admin->avatar, 'admin');
            $admin->avatar = $this->uploadImage($request, 'image', 'admin');
        }

        return $admin->save();
    }

    public function updatePassword($admin, $request)
    {
        if (\Hash::check($request->old_password, $admin->password)) {
            $admin->password = \Hash::make($request->password);
            $admin->save();
            return true;
        }

        return false;
    }

    public function search($request)
    {
        $key = explode(' ', $request['search']);
        return Category::where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->orderBy('id')->paginate(10);
    }
}
