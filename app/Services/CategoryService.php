<?php

namespace App\Services;
use App\Http\Controllers\Custom\CategoryController;
use App\Models\Category;
use App\Http\Traits\ImageUpload;
use Illuminate\Support\Facades\Request;

class CategoryService
{
    use ImageUpload;

    public function create($model, $request)
    {
        $category = new Category;
        $category->name = $request->name;
        if ($request->hasFile('image')) {
            $category->image = $this->uploadImage($request, 'image', 'category');
        }

        return $model->categories()->save($category);
    }

    public function update($category, $request)
    {
        $category->name = $request['name'];
        if ($request->hasFile('image')) {
            // $path = 'category/'.$category->image;
            $this->removeImage($category->image, 'category');
            // Storage::delete($path);
            $category->image = $this->uploadImage($request, 'image', 'category');
        }

        return $category->save();
    }

    public function search($request)
    {
        $key = explode(' ', $request['search']);
        return Category::where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->orderBy('id')->paginate(10);
    }

    public function categoryDetail($request,$detail)
    {
       $categories = Category::where('id',$detail)->with('products')->first();
//       dd($categories);

//       dd($categories->products);
//       foreach($categories->products as $pro){
//           dd($pro->name);
////           foreach($pro->images as $img){
////               dd($img->image);
////           };
//    };
//        foreach ($categories)

        return view('category.catepro',compact('categories'));
    }
}
