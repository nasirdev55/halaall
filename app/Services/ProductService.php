<?php

namespace App\Services;

use App\Models\Product;
use App\Models\ProductImage;
use App\Http\Traits\ImageUpload;
use Illuminate\Support\Facades\Request;

class ProductService
{
    use ImageUpload;

    public function productDetail($request,$detail)
    {
        $product = Product::where('id',$detail)->first();
//        dd($product->name);

        return view('products.productdetail',compact('product'));
    }
    public function index()
    {
        return Product::where('status', '!=', 'pending')->orderBy('id', 'desc')->paginate(10);
    }

    public function create($request)
    {
        $product = Product::create([
            'restaurant_id' => (Request::wantsJson())?auth()->id():auth()->guard('restaurant')->id(),
            'category_id'   => $request->category_id,
            'name'          => $request->name,
            'price'         => $request->price,
            'description'   => $request->description,
            // 'delivery_time' => isset($request->delivery_time)?$request->delivery_time:0,
            'is_featured'   => isset($request->is_featured)?$request->is_featured:0,
            'is_available'  => isset($request->is_available)?$request->is_available:0,
        ]);

        if ($request->hasFile('images')) {
            $images = $this->uploadMultipleImages($request, 'images', 'product');
            foreach ($images as $key => $image) {
               ProductImage::create([
                    'product_id' => $product->id,
                    'image'      => $image
               ]);
            }
        }

        return $product;
    }

    public function update($request)
    {
        $restaurantId = (Request::wantsJson())?auth()->id():auth()->guard('restaurant')->id();
        $product = Product::with('images')->where('id',$request->product_id)->where('restaurant_id', $restaurantId)->first();

        $product->name        = $request->name;
        $product->price       = $request->price;
        $product->description = $request->description;
        $product->save();

        if ($request->hasFile('images')) {
            $images = $this->uploadMultipleImages($request, 'images', 'product');
            foreach ($images as $key => $image) {
               ProductImage::create([
                'product_id' => $product->id,
                'image' => $image
               ]);
            }
        }

        return $product;
    }

    public function updateStatus($product, $status)
    {
        if (array_key_exists($status, Product::$statuses)) {
            $product->status = $status;
            $product->save();
            return true;
        }
        throw new \Exception(" Invalid status passed", 1);
    }

    public function requests()
    {
        return Product::with('category', 'restaurant')->where('status', 'pending')->orderBy('id')->paginate(10);
    }

    public function filter($request)
    {
        $key = explode(' ', $request['term']);
        return Product::with('category')->where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->orderBy('id')->paginate(10);
    }

    public function search($request)
    {
        $key = explode(' ', $request['search']);
        return Product::with('category', 'restaurant')->where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->orderBy('id')->paginate(10);
    }

    public function requestSearch($request)
    {
        $key = explode(' ', $request['search']);
        return Product::with('category', 'restaurant')->where('status', 'pending')->where(function ($q) use ($key) {
            foreach ($key as $value) {
                $q->orWhere('name', 'like', "%{$value}%");
            }
        })->orderBy('id')->get();
    }

    public function requestUpdateStatus($product, $status)
    {
        if (array_key_exists($status, Product::$statuses)) {
            $product->status = $status;
            $product->save();
            return true;
        }
        throw new Exception(" Invalid status passed", 1);
    }
}
