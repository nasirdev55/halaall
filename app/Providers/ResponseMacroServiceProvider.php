<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Response::macro('success', function ($message, $data=null, $statusCode=200) {
            $response = array(
                'status' => true,
                'response_code' => $statusCode,
                'message' => $message,
            );
            if (!is_null($data)) {
                $response['data'] = $data;
            }
            return Response::make($response, $statusCode);
        });

        Response::macro('successWithToken', function ($token, $message=null, $data=null,$statusCode=200) {
            return Response::make([
                'status'  => true,
                'response_code' => $statusCode,
                'message' => $message,
                'token'   => $token,
                'data'    => $data
            ], $statusCode);
        });

        Response::macro('validationError', function ($message, $statusCode=422) {
            return Response::make([
                'status'        => false,
                'response_code' => $statusCode,
                'message'       => $message
            ], $statusCode);
        });

        Response::macro('error', function ($message, $statusCode=404) {
            return Response::make([
                'status'  => false,
                'response_code' => $statusCode,
                'message' => $message
            ], $statusCode);
        });

        Response::macro('paginated', function ($message, $statusCode=404) {
            return Response::make([
                'status'  => false,
                'response_code' => $statusCode,
                'message' => $message
            ], $statusCode);
        });
    }
}
