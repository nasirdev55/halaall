<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\Admin;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Admin::updateOrCreate([
            'email' => 'admin@justhalaall.com'
        ],[
            'first_name' => 'Admin',
            'last_name'  => 'Just Halaall',
            'phone'      => '01759412381',
            'avatar'     => 'def.png',
            'password'   => bcrypt(123456),
        ]);
    }
}
