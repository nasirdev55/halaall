<?php

use Illuminate\Database\Seeder;

use App\Models\BusinessSetting;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = array(
            'restaurant_name' => '',
            'footer_text' => '',
            'logo' => ''
        );
        // dd($settings);
        foreach ($settings as $key => $value) {
            BusinessSetting::create([
                'key' => $key,
                'value' => $value
            ]);
        }
    }
}
