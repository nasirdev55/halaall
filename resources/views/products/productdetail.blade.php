@extends('layouts.site.app')

@section('title','Product Details')

@push('css_or_js')


{{--     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">--}}


@endpush
<!--Section: Block Content-->

<!--Section: Block Content-->
@section('content')
    <div class="container mt-7" style="margin-top: 50px;">

        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body" style="margin-bottom: 40px;">
                                <div class="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                                    @foreach($product->images as $pro)
                                        <img
                                            src="{{asset('storage/product/'.$pro->image)}}"
                                            class="img-fluid"
                                        />
                                    @endforeach
                                    <a href="#!">
                                        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body" style="margin-bottom: 50px;">
                                <form class="create-form" enctype="multipart/form-data" method="post">

                                    @csrf

                                    <h5>{{$product->name}}</h5>
                                    {{--                               <p class="mb-2 text-muted text-uppercase small">Shirts</p>--}}
                                    <div class="rating-container theme-krajee-fas rating-sm rating-animate">
                                        <div class="clear-rating clear-rating-active" title="Clear">
                                            <i class="fas fa-minus-circle"></i></div>
                                        <div class="res-banner-ratings">
                                            <div class="res-banner-rate-block">
                                                Ratings <br>

                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>


                                            </div>
                                        </div>
                                        <input type="hidden" name="price" value="{{$product->price}}">
                                        <p><span class="mr-1"><strong>${{$product->price}}</strong></span></p>
                                        <p class="pt-1">
                                            {{$product->description}}
                                        </p>
                                        {{--                               <div class="table-responsive">--}}
                                        {{--                                   <table class="table table-sm table-borderless mb-0">--}}
                                        {{--                                       <tbody>--}}
                                        {{--                                       <tr>--}}
                                        {{--                                           <th class="pl-0 w-25" scope="row"><strong>Model</strong></th>--}}
                                        {{--                                           <td>Shirt 5407X</td>--}}
                                        {{--                                       </tr>--}}
                                        {{--                                       <tr>--}}
                                        {{--                                           <th class="pl-0 w-25" scope="row"><strong>Color</strong></th>--}}
                                        {{--                                           <td>Black</td>--}}
                                        {{--                                       </tr>--}}
                                        {{--                                       <tr>--}}
                                        {{--                                           <th class="pl-0 w-25" scope="row"><strong>Delivery</strong></th>--}}
                                        {{--                                           <td>USA, Europe</td>--}}
                                        {{--                                       </tr>--}}
                                        {{--                                       </tbody>--}}
                                        {{--                                   </table>--}}
                                        {{--                               </div>--}}
                                        <hr>
                                        <div class="table-responsive mb-2">
                                            <table class="table table-sm table-borderless">
                                                <tbody>


                                                {{--                                           <td class="pb-0">Select size</td>--}}

                                                <tr>
                                                    <td class="pl-0 pb-0 w-25">Quantity</td>
                                                    <td>

                                                        <div class="input-group">
                                    <span class="input-group-btn">
                                        <button type="button" class="quantity-left-minus btn btn-danger btn-number"
                                                data-type="minus" data-field="">
                                          <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>

                                                            <input type="text" id="quantity" name="quantity"
                                                                   class="form-control input-number" value="10" min="1"
                                                                   max="100">
                                                            <span class="input-group-btn">
                                        <button type="button" class="quantity-right-plus btn btn-success btn-number"
                                                data-type="plus" data-field="">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                                        </div>
                                                    </td>


                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        {{--
                                         <button type="button" class="btn btn-primary btn-md mr-1 mb-2 waves-effect waves-light">Buy now</button>--}}

                                        <button type="submit"
                                                class="btn btn-primary btn-md mr-1 mb-2 waves-effect waves-light">
                                            <i class="fas fa-shopping-cart pr-2"></i>Add to
                                            cart
                                        </button>


                                    </div>


                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
        </div>
        @endsection

        @push('script')
            <script>
                $(document).ready(function () {

                    var quantitiy = 0;
                    $('.quantity-right-plus').click(function (e) {

                        // Stop acting like a button
                        e.preventDefault();
                        // Get the field name
                        var quantity = parseInt($('#quantity').val());



                        // If is not undefined

                        $('#quantity').val(quantity + 1);


                        // Increment

                    });

                    $('.quantity-left-minus').click(function (e) {
                        // Stop acting like a button
                        e.preventDefault();
                        // Get the field name
                        var quantity = parseInt($('#quantity').val());

                        // If is not undefined

                        // Increment
                        if (quantity > 0) {
                            $('#quantity').val(quantity - 1);
                        }
                    });

                });


                $('.create-form').on('submit', function (e) {
                    e.preventDefault();
                    var data = $('.create-form').serialize();
                    // data = new FormData(data[0]);
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url: '{{route('productCart',['product_id'=> $product->id])}}',
                        data: data,
                        success: function (data) {
                            // $.unblockUI();
                            if (data.status == 1) {


//                     swal("Successfully", "Added to Your Cart", "success", {
//                         button: "OK",
//                     });
// //                        successMsg(data.message);
// //                        window.location.href = data.url;
//                     window.location.reload();
                            }
                            if (data.status == 0) {
                                // swal("Already", "Product is in Your Cart", "error", {
                                //     button: "OK",
                                // });
//                        errorMsg(data.message);
                            }
                        },
                        error: function (data) {
                            console.log('error');
                            // $.unblockUI();

                        }

                    });
                });

            </script>
    @endpush
