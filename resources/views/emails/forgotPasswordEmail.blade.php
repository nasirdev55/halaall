<!DOCTYPE html>
<html>
<head>
    <title>Forgot Password Email</title>
</head>
<body>
    <h1>{{ $user->name }}</h1>
    <p>Kindly use this code to activate your <a href="{{ config('app.url') }}">{{ config('app.name') }}.</a> Your verification code is{{ $user->verification_code }}</p>
   
    <p>Thank you</p>
</body>
</html>