@extends('layouts.site.app')

@section('title','Category Products')

@push('css_or_js')

@endpush

@section('content')
    <div class="restaurant-banner row">
        <!-- Restaurant Banner Left Starts -->
        <!--                    <div class="res-banner-left col-md-3">
        <div class="res-banner-img bg-img"
             style="background-image: url(https://feedme.nc/images/shops/1596016193_avatar.jpg);"></div>
         </div>-->
        <!-- Restaurant Banner Left Ends -->
        <div class="banner-inner">
            <!-- Restaurant Banner Center Starts -->
            <div class="res-banner-center col-md-10">
                <div class="row shop-details">
                    <div class="shop-detail-item col-md-4">
                        <h4 class="">{{$categories->name}}</h4>
                    </div>
                    <div class="shop-detail-item col-md-3">
                        lahore akjcdl
                    </div>
                    <div class="shop-detail-item col-md-2">
                        <div class="res-banner-ratings">
                            <div class="res-banner-rate-block">
                                Ratings <br>

                                <i class="ion-android-star ratingActive"></i>
                                <i class="ion-android-star ratingActive"></i>
                                <i class="ion-android-star ratingActive"></i>
                                <i class="ion-android-star ratingActive"></i>
                                <i class="ion-android-star ratingActive"></i>


                            </div>
                        </div>
                    </div>
                    <div class="shop-detail-item col-md-3">
                        <div class="img-set">
                            <img src="{{ asset($categories->image) }}" alt="">
                        </div>
{{--                        Estimated delivery time <p class="">12Mins</p>--}}
                    </div>
                </div>
                <!-- <div class="col-md-12 shop-desc">
                    <p>wonderful food</p>
                </div> -->

                <!-- Restaurant Banner Ratings Ends -->

            </div>
            <!-- Restaurant Banner Center Ends -->
            <!-- Restaurant Banner Right Starts -->
        </div>

    </div>



        <div class="row" style="margin-top: 40px">
            @foreach($categories->products as $pro)

            <div class="food-list-section row">
                <div class="col-md-3"></div>
                <a href="{{route('productDetail',['detail'=>$pro->id])}}">
                <div class="col-md-6">

                    <div class="row">
                            <div class="card-inner">
                                <div class="card-s">
                                    @foreach($pro->images as $img)
                                    <div class="img-set">
{{--                                        <img src="http://justhalaall.com/storage/product/1631288432613b7c70e2f44.png" alt="">--}}
                                        <img src="{{asset('storage/product/'.$img->image)}}" alt="">
                                    </div>
                                    @endforeach
                                    <div class="food-detail">
                                        <div class="name-favourtbtn">
                                            <p>{{$pro->name}}</p>
{{--                                            <p><button class="add-to-cartbtn Favourit"><i class="ion-heart" style="font-size: 20px;"></i></button></p>--}}
                                        </div>
                                        <div class="rating">
                                            <div class="res-banner-rate-block">
                                                <!-- <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i> -->
                                            </div>
                                        </div>
                                        <div class="prise-addbtn">
                                            <p class="price">${{$pro->price}}</p>
                                            <p><button class="add-to-cartbtn"><i class="ion-plus" style="font-size: 20px;"></i></button></p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>

                </div>
                </a>
                <div class="col-md-3"></div>

            </div>

            @endforeach



    </div>
@endsection

@push('script')

@endpush
