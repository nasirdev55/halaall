<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-5">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navgation-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo">
                        <a class="navbar-brand logoback" href="{{ route('web.landing') }}">
                            <img src="{{ asset('assets/site/img/app/logo.png') }}" height="50"
                                alt="" data-retina="true" class="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="collapse navbar-collapse" id="navgation-1">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="nav navbar navbar-right addressform">
                                <li>

                                </li>
                            </ul>
                        </div>
                        <div class="col-md-12">
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a href="{{ route('web.search') }}">
                                        <i class="ion-search"></i> Search
                                    </a>
                                </li>
                                <li>
                                    <a href=""><i class="ion-help-buoy"></i> Help</a>
                                </li>

                                @guest
                                    <li> <a href="javascript:void(0);" class="signinform">Login</a></li>
                                    @if (Route::has('register'))
                                        <li><a href="javascript:void(0);" class="active signupform">Register</a></li>
                                    @endif
                                @endguest

                                <li class="dropdown">
                                    <a href=""><span class="cart-count">0</span> Cart</a>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                        aria-haspopup="true" aria-expanded="false">
                                        en
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="">ar</a></li>
                                    </ul>
                                </li>
                                @auth
                                    <li class="dropdown">
                                        <a id="" class="dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }}
                                            <span class="caret"></span>
                                        </a>
                                        <ul class="dropdown-menu" aria-labelledby="">
                                            <li>
                                                <a class="" href="{{ route('logout') }}"
                                                   onclick="event.preventDefault();
                                                                 document.getElementById('logout-form').submit();">
                                                    {{ __('Logout') }}
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                    @csrf
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                @endauth
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>
