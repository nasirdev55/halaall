<footer class="footer" style="z-index: 99;">
    <div class="container">
        <!-- Foot Top Starts -->
        <div class="foot-top row">
            <!-- Foot Block Starts -->
            <div class="foot-block col-md-3 col-sm-12 col-xs-12">
                <h5 class="foot-tit">ABOUT US</h5>
                <div>
                    <a href="{{ route('web.aboutus') }}" class="foot-item">About us</a>
                    
                    <a href="{{ route('web.privacy') }}" class="foot-item">Privacy</a>

                </div>
            </div>
            <!-- Foot Block Ends -->
            <!-- Foot Block Starts -->
            <div class="foot-block col-md-3 col-sm-12 col-xs-12">
                <h5 class="foot-tit">SUPPORT</h5>
                <div>
                    <a href="{{ route('web.faq') }}" class="foot-item">FAQ</a>
                </div>
            </div>
            <!-- Foot Block Ends -->
            <!-- Foot Block Starts -->
            <div class="foot-block col-md-3 col-sm-12 col-xs-12">
                <h5 class="foot-tit">Terms and Conditions</h5>
                <div>
                    <a href="{{ route('web.terms') }}" class="foot-item">Terms and Conditions</a>




                </div>
            </div>
            <!-- Foot Block Ends -->
            <!-- Foot Block Starts -->
            <div class="foot-block col-md-3 col-sm-12 col-xs-12">
                <div class="foot-download-img-sec">

                    <a href="" class="foot-download-img" target="_blank"><img src="{{ asset('assets/site/img/play-store.png') }}"></a>
                </div>
                <div class="foot-download-img">
                    <a href="" class="foot-download-img" target="_blank"><img src="{{ asset('assets/site/img/android.png') }}"></a>

                </div>
            </div>
            <!-- Foot Block Ends -->
        </div>
        <!-- Foot Top Ends -->
        <!-- Foot Bottom Starts -->
        <div class="foot-btm row">
            <div class="col-md-4">
                <div class="foot-logo"><img src="{{ asset('assets/site/img/app/logo.png') }}">
                </div>
            </div>
            <div class="col-md-4 text-center">
                <p class="copy-txt">© 2021 site</p>
            </div>
            <div class="foot-social col-md-4 text-right">
                <a href="" target="_blank" class="foot-social-item"><i class="fa fa-facebook"></i></a>

                <a href="" target="_blank" class="foot-social-item"><i class="fa fa-instagram"></i></a>

            </div>
        </div>
        <!-- Foot Bottom Ends -->
    </div>
</footer>