@foreach($products as $key=> $product)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>
            <span class="d-block font-size-sm text-body">
                 <a href="{{route('admin.products.show',[$product['id']])}}">
                   {{$product->name}}
                 </a>
            </span>
        </td>
        <td>
            <span class="d-block font-size-sm text-body">
                 {{$product->description}}
            </span>
        </td>
        <td>
            <span class="d-block font-size-sm text-body">
                {{$product->category->name}}
            </span>
        </td>
        <td>
            <span class="d-block font-size-sm text-body">
                {{$product->restaurant->name}}
            </span>
        </td>
        <td>
            <!-- Dropdown -->
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    <i class="tio-settings"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item"
                       href="{{route('admin.products.request.update.status',[$product['id'], 'approved'])}}">{{trans('messages.approve')}}</a>
                    <a class="dropdown-item" href="javascript:"
                       onclick="route_alert('{{route('admin.products.request.update.status',[$product['id'], 'rejected'])}}','Want to delete this item ?')">{{trans('messages.reject')}}</a>
                </div>
            </div>
            <!-- End Dropdown -->
        </td>
    </tr>
@endforeach