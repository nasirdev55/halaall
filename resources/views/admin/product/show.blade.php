@extends('layouts.admin.app')

@section('title','Product Details')

@push('css_or_js')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endpush

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i class="tio-add-circle-outlined"></i> {{trans('messages.product')}} {{trans('messages.details')}}</h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form id="product_form">
                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('messages.name')}}</label>
                        <input type="text" name="name" class="form-control" placeholder="New Product" value="{{ $product->name }}" readonly>
                    </div>
                    <div class="form-group">
                        <label class="input-label" for="exampleFormControlInput1">{{trans('messages.short')}} {{trans('messages.description')}}</label>
                        <textarea type="text" name="description" class="form-control">{{ $product->description }}</textarea>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlInput1">{{trans('messages.price')}}<span class="input-label-secondary">*</span></label>
                                <input type="number" min="1" max="100000" step="0.01" value="1" name="price" class="form-control"
                                       placeholder="Ex : 100" value="{{ $product->price }}" readonly>
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlSelect1">{{trans('messages.category')}}<span
                                        class="input-label-secondary">*</span></label>
                                <select name="category_id" class="form-control js-select2-custom" readonly>
                                    <option value="">---{{trans('messages.select')}}---</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}" {{ ($category->id == $product->category_id)?'selected':'' }}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlSelect1">{{trans('messages.featured')}}</label>
                                <select name="featured" class="form-control js-select2-custom" readonly>
                                    <option value="0" {{ (!$product->is_featured)?'selected':'' }}>No</option>
                                    <option value="1" {{ ($product->is_featured)?'selected':'' }}>Yes</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-6">
                            <div class="form-group">
                                <label class="input-label" for="exampleFormControlInput1">{{trans('messages.available')}}</label>
                                <select name="available" class="form-control js-select2-custom" readonly>
                                    <option value="1" {{ ($product->is_available)?'selected':'' }}>Yes</option>
                                    <option value="0" {{ (!$product->is_available)?'selected':'' }}>No</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>
                            {{trans('messages.product')}} 
                            {{trans('messages.image')}}
                        </label> 
                        <center id="image-viewer-section" class="pt-2">
                            <img style="width: 30%;border: 1px solid; border-radius: 10px;" id="viewer"
                                         src="{{ $product->singleImage() }}"
                                         onerror="this.src='{{asset('assets/admin/img/900x400/img1.jpg')}}'" alt="image"/>
                        </center>
                    </div>
                    <hr>
                    <button type="button" class="btn btn-primary" onclick="window.history.back()">{{trans('messages.back')}}</button>
                </form>
            </div>
        </div>
    </div>

@endsection



