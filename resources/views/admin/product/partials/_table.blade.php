@foreach($products as $key=>$product)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>
            <span class="d-block font-size-sm text-body">
                 <a href="{{route('admin.products.show',[$product['id']])}}">
                   {{$product['name']}}
                 </a>
            </span>
        </td>
        <td>
            {{$product['description']}}
        </td>
        <td>
            <span class="d-block font-size-sm text-body">
                {{ optional($product->category)->name}}
            </span>
        </td>
        <td>
            <span class="d-block font-size-sm text-body">
                {{ optional($product->restaurant)->name}}
            </span>
        </td>
        <td> 
            <div style="padding: 10px;border: 1px solid;">
                </span>{{trans('messages.'.$product['status'])}}
            </div>
        </td>
        <td>
            <!-- Dropdown -->
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    <i class="tio-settings"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item"
                       href="{{route('admin.products.edit',[$product['id']])}}">{{trans('messages.edit')}}</a>
                    <a class="dropdown-item" href="javascript:"
                       onclick="form_alert('product-{{$product['id']}}','Want to delete this item ?')">{{trans('messages.delete')}}</a>
                    <form action="{{--route('admin.products.delete',[$product['id']])--}}"
                          method="post" id="product-{{$product['id']}}">
                        @csrf @method('delete')
                    </form>
                </div>
            </div>
            <!-- End Dropdown -->
        </td>
    </tr>
@endforeach