@extends('layouts.admin.app')

@section('title','Add new restaurant')

@push('css_or_js')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/dropify/dropify.min.css') }}">
@endpush

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i
                            class="tio-add-circle-outlined"></i> {{trans('messages.add')}} {{trans('messages.new')}} {{trans('messages.restaurant')}}
                    </h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="{{route('admin.restaurants.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="exampleFormControlInput1">{{trans('messages.name')}}</label>
                                <input type="text" name="name" class="form-control" placeholder="New Restaurant" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="exampleFormControlInput1">{{trans('messages.email')}}</label>
                                <input type="email" name="email" class="form-control"
                                       placeholder="EX : example@example.com" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="exampleFormControlInput1">{{trans('messages.phone')}}</label>
                                <input type="text" name="phone" class="form-control" placeholder="EX : +923001234567" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="exampleFormControlInput1">{{trans('messages.password')}}</label>
                                <input type="text" name="password" class="form-control" placeholder="Password" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        {{--
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="status">{{trans('messages.status')}}</label>
                                <select id="status" class="js-select2-custom" name="status">
                                    <option value="1">{{trans('messages.active')}}</option>
                                    <option value="0">{{trans('messages.disabled')}}</option>
                                </select>
                            </div>
                        </div>
                        --}}
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" for="">{{trans('messages.address')}}</label>
                                <input type="text" name="address" class="form-control" placeholder="Ex : Lahore Pakistan"
                                       required>
                            </div>
                        </div>
                        <div class="col-6 d-flex">
                            <div class="form-group">
                                <label class="input-label">{{trans('messages.delivery_time')}}</label>
                                <div class="col-12 pr-10 pl-0">
                                    <input type="text" name="delivery_time" class="form-control" placeholder="delivery_time">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="input-label">{{trans('messages.delivery_charges')}}</label>
                                <div class="col-12 pr-2 pl-0">
                                    <input type="text" name="delivery_charges" class="form-control" placeholder="delivery_charges">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label">{{trans('messages.working_hours')}}</label>
                                <div class="d-flex">
                                    <div class="col-6 pr-2 pl-0">
                                        <input type="time" name="start_time" class="form-control" placeholder="Start Time"
                                               >
                                    </div>
                                    <div class="col-6 pl-2 pr-0">
                                        <input type="time" name="end_time" class="form-control" placeholder="End Time"
                                               >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <label>{{trans('messages.logo')}}</label><small style="color: red">* ( {{trans('messages.ratio')}} 3:1 )</small>
                            <div class="custom-file">
                                <input type="file" name="logo" id="customFileEg1" class="custom-file-input" required>
                                <label class="custom-file-label" for="customFileEg1">{{trans('messages.choose')}} {{trans('messages.file')}}</label>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <hr>
                                <center>
                                    <img style="width: 30%;border: 1px solid; border-radius: 10px;" id="viewer"
                                         src="{{asset('assets/admin/img/900x400/img1.jpg')}}" alt="image"/>
                                </center>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary">{{trans('messages.submit')}}</button>
                </form>
            </div>
            <!-- End Table -->
        </div>
    </div>

@endsection

@push('script_2')
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#viewer').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#customFileEg1").change(function () {
        readURL(this);
    });
</script>
@endpush
