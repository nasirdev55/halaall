@extends('layouts.admin.app')

@section('title','Restaurant Details')

@section('content')
    <div class="content container-fluid">
        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-sm mb-2 mb-sm-0">
                    <h1 class="page-header-title"><i
                            class="tio-add-circle-outlined"></i>  {{trans('messages.restaurant')}}
                            {{trans('messages.details')}}
                    </h1>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row gx-2 gx-lg-3">
            <div class="col-sm-12 col-lg-12 mb-3 mb-lg-2">
                <form action="">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="exampleFormControlInput1">{{trans('messages.name')}}</label>
                                <input type="text" name="name" class="form-control" placeholder="New Restaurant" value="{{ $restaurant->name }}" readonly>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="exampleFormControlInput1">{{trans('messages.email')}}</label>
                                <input type="email" name="email" class="form-control"
                                       placeholder="EX : example@example.com" value="{{ $restaurant->email }}" readonly >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="exampleFormControlInput1">{{trans('messages.phone')}}</label>
                                <input type="text" name="phone" class="form-control" placeholder="EX : +923001234567" value="{{ $restaurant->phone }}" readonly>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label"
                                       for="status">{{trans('messages.status')}}</label>
                                <select id="status" class="js-select2-custom" name="status">
                                    <option value="pending" {{ ($restaurant->status == 'pending')?"selected":"" }}>{{trans('messages.pending')}}</option>
                                    <option value="approved" {{ ($restaurant->status == 'approved')?"selected":"" }}>{{trans('messages.approved')}}</option>
                                    <option value="rejected" {{ ($restaurant->status == 'rejected')?"selected":"" }}>{{trans('messages.rejected')}}</option>
                                    <option value="disabled" {{ ($restaurant->status == 'disabled')?"selected":"" }}>{{trans('messages.disabled')}}</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label" for="">{{trans('messages.address')}}</label>
                                <input type="text" name="address" class="form-control" placeholder="Ex : Lahore Pakistan"
                                      value="{{ $restaurant->address }}" readonly>
                            </div>
                        </div>
                        <div class="col-6 d-flex">
                            <div class="form-group">
                                <label class="input-label">{{trans('messages.delivery_time')}}</label>
                                <div class="col-12 pr-10 pl-0">
                                    <input type="text" name="delivery_time" class="form-control" placeholder="delivery_time" value="{{ $restaurant->delivery_time }}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="input-label">{{trans('messages.delivery_charges')}}</label>
                                <div class="col-12 pr-2 pl-0">
                                    <input type="text" name="delivery_charges" class="form-control" placeholder="delivery_charges" value="{{ $restaurant->delivery_charges }}" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="input-label">{{trans('messages.working_hours')}}</label>
                                <div class="d-flex">
                                    <div class="col-6 pr-2 pl-0">
                                        <input type="time" name="start_time" class="form-control" placeholder="Start Time" value="{{ optional($restaurant->timings)->start_time }}"
                                                readonly>
                                    </div>
                                    <div class="col-6 pl-2 pr-0">
                                        <input type="time" name="end_time" class="form-control" placeholder="End Time" value="{{ optional($restaurant->timings)->end_time }}"
                                               readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <hr>
                                <center>
                                    <img style="width: 30%;border: 1px solid; border-radius: 10px;" id="viewer"
                                         src="{{ $restaurant->logo }}" alt="image"/>
                                </center>
                            </div>
                        </div>
                    </div>
                </form>
                <button type="button" class="btn btn-primary" onclick="window.history.back()">{{trans('messages.back')}}</button>
            </div>
            <!-- End Table -->
        </div>
    </div>

@endsection
