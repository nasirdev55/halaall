@foreach($restaurants as $key=>$restaurant)
    <tr>
        <td>{{ $loop->iteration }}</td>
        <td>
            <span class="d-block font-size-sm text-body">
                 <a href="{{route('admin.restaurants.show',[$restaurant['id']])}}">
                   {{$restaurant['name']}}
                 </a>
            </span>
        </td>
        <td>
            <div style="height: 100px; width: 100px; overflow-x: hidden;overflow-y: hidden">
                <img src="{{ $restaurant['logo'] }}" style="width: 100px"
                     onerror="this.src='{{asset('assets/admin/img/160x160/img2.jpg')}}'">
            </div>
        </td>
        <td>
            <div style="padding: 10px;border: 1px solid;">
                </span>{{trans('messages.'.$restaurant['status'])}}
            </div>
        </td>
        <td>
            <!-- Dropdown -->
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    <i class="tio-settings"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item"
                       href="{{route('admin.restaurants.edit',[$restaurant['id']])}}">{{trans('messages.edit')}}</a>
                    <a class="dropdown-item" href="javascript:"
                       onclick="form_alert('restaurant-{{$restaurant['id']}}','Want to delete this item ?')">{{trans('messages.delete')}}</a>
                    <form action="{{route('admin.restaurants.destroy',[$restaurant['id']])}}"
                          method="post" id="restaurant-{{$restaurant['id']}}">
                        @csrf @method('delete')
                    </form>
                </div>
            </div>
            <!-- End Dropdown -->
        </td>
    </tr>
@endforeach
