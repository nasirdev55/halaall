@foreach($categories as $key=>$category)
    <tr>
        <td>{{$loop->iteration}}</td>
        <td>
        <span class="d-block font-size-sm text-body">
            {{$category['name']}}
        </span>
        </td>
        <td>
            <div style="height: 100px; width: 100px; overflow-x: hidden;overflow-y: hidden">

                <img src="{{$category->image}}" style="width: 100px"
                     onerror="this.src='{{asset('assets/admin/img/160x160/img2.jpg')}}'">
            </div>
        </td>
        <td>
            @if($category['status']==1)
                <div style="padding: 10px;border: 1px solid;cursor: pointer"
                     onclick="location.href='{{route('admin.categories.status',[$category['id'],0])}}'">
                    <span class="legend-indicator bg-success"></span>{{trans('messages.active')}}
                </div>
            @else
                <div style="padding: 10px;border: 1px solid;cursor: pointer"
                     onclick="location.href='{{route('admin.categories.status',[$category['id'],1])}}'">
                    <span class="legend-indicator bg-danger"></span>{{trans('messages.disabled')}}
                </div>
            @endif
        </td>
        <td>
            <!-- Dropdown -->
            <div class="dropdown">
                <button class="btn btn-secondary dropdown-toggle" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                    <i class="tio-settings"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item"
                       href="{{route('admin.categories.edit',[$category['id']])}}">{{trans('messages.edit')}}</a>
                    <a class="dropdown-item" href="javascript:"
                       onclick="form_alert('category-{{$category['id']}}','Want to delete this category')">{{trans('messages.delete')}}</a>
                    <form action="{{route('admin.categories.destroy',[$category['id']])}}"
                          method="post" id="category-{{$category['id']}}">
                        @csrf @method('delete')
                    </form>
                </div>
            </div>
            <!-- End Dropdown -->
        </td>
    </tr>
@endforeach