<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Title -->
    <title>Restaurant | Login</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&amp;display=swap" rel="stylesheet">
    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="{{asset('assets/admin')}}/css/vendor.min.css">
    <link rel="stylesheet" href="{{asset('assets/admin')}}/vendor/icon-set/style.css">
    <link rel="stylesheet" href="{{asset('assets/admin')}}/css/custom.css">
    <!-- CSS Front Template -->
    <link rel="stylesheet" href="{{asset('assets/admin')}}/css/theme.minc619.css?v=1.0">
    <link rel="stylesheet" href="{{asset('assets/admin')}}/css/toastr.css">
</head>

<body>
<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main" class="main">
    <div class="position-fixed top-0 right-0 left-0 bg-img-hero"
         style="height: 100%; background-image: url({{asset('assets/admin')}}/svg/components/login-background.png);">
    </div>

    <!-- Content -->
    <div class="container py-5 py-sm-7">
        <a class="d-flex justify-content-center mb-5" href="javascript:">
            <img class="z-index-2"
                 onerror="this.src='{{asset('assets/admin/img/160x160/img2.jpg')}}'"
                 src="{{asset('storage/app/restaurant')}}/{{--\App\Model\BusinessSetting::where(['key'=>'logo'])->first()->value --}}" alt="Image Description" style="height: 100px;">
        </a>

        <div class="row justify-content-center">
            <div class="col-md-7 col-lg-5" id="dynamo">
                <!-- Card -->
                <div class="card card-lg mb-5">
                    <div class="card-body">
                        <!-- Form -->
                        <form class="js-validate" action="{{route('restaurant.auth.login')}}" id="do-restaurant-login" method="post">
                            @csrf

                            <div class="text-center">
                                <div class="mb-5">
                                    <h1 class="display-4">Restaurant Sign in</h1>
                                </div>
                                {{--<a class="btn btn-lg btn-block btn-white mb-4" href="#">
                                    <span class="d-flex justify-content-center align-items-center">
                                      <img class="avatar avatar-xss mr-2"
                                           src="{{asset('assets/admin')}}/svg/brands/google.svg" alt="Image Description">
                                      Sign in with Google
                                    </span>
                                </a>
                                <span class="divider text-muted mb-4">OR</span>--}}
                            </div>

                            <!-- Form Group -->
                            <div class="js-form-message form-group">
                                <label class="input-label" for="signinSrEmail">Your email</label>

                                <input type="email" class="form-control form-control-lg" name="email" id="signinSrEmail"
                                       tabindex="1" placeholder="email@address.com" aria-label="email@address.com"
                                       required data-msg="Please enter a valid email address.">
                            </div>
                            <!-- End Form Group -->

                            <!-- Form Group -->
                            <div class="js-form-message form-group">
                                <label class="input-label" for="signupSrPassword" tabindex="0">
                                    <span class="d-flex justify-content-between align-items-center">
                                      Password
                                    </span>
                                </label>

                                <div class="input-group input-group-merge">
                                    <input type="password" class="js-toggle-password form-control form-control-lg"
                                           name="password" id="signupSrPassword" placeholder="8+ characters required"
                                           aria-label="8+ characters required" required
                                           data-msg="Your password is invalid. Please try again."
                                           data-hs-toggle-password-options='{
                                                     "target": "#changePassTarget",
                                            "defaultClass": "tio-hidden-outlined",
                                            "showClass": "tio-visible-outlined",
                                            "classChangeTarget": "#changePassIcon"
                                            }'>
                                    <div id="changePassTarget" class="input-group-append">
                                        <a class="input-group-text" href="javascript:">
                                            <i id="changePassIcon" class="tio-visible-outlined"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Form Group -->

                            <!-- Checkbox -->
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="termsCheckbox"
                                           name="remember">
                                    <label class="custom-control-label text-muted" for="termsCheckbox">
                                        Remember me
                                    </label>
                                </div>
                            </div>
                            <!-- End Checkbox -->

                            <button type="submit" class="btn btn-lg btn-block btn-primary">Sign in</button>
                            {{-- 
                            <div class="mt-4">
                                <span><a href="">Forgot Password</a></span>
                            </div>
                            --}}
                            <div class="mt-4">
                                <span>Don't you have an account?</span>
                                <span><a href="javascript:" id="res-register">Register</a></span>
                            </div>
                        </form>
                        <!-- End Form -->
                        <form action="{{route('restaurant.auth.register')}}" class="res-hide-form" method="post" id="res-register-form" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="input-label"
                                               for="exampleFormControlInput1">{{trans('messages.name')}}</label>
                                        <input type="text" name="name" class="form-control" placeholder="New Restaurant" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="input-label"
                                               for="exampleFormControlInput1">{{trans('messages.email')}}</label>
                                        <input type="email" name="email" class="form-control"
                                               placeholder="EX : example@example.com" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="input-label"
                                               for="exampleFormControlInput1">{{trans('messages.phone')}}</label>
                                        <input type="text" name="phone" class="form-control" placeholder="EX : +923001234567" required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="input-label"
                                               for="exampleFormControlInput1">{{trans('messages.password')}}</label>
                                        <input type="text" name="password" class="form-control" placeholder="Password" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                {{--
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="input-label"
                                               for="status">{{trans('messages.status')}}</label>
                                        <select id="status" class="js-select2-custom" name="status">
                                            <option value="1">{{trans('messages.active')}}</option>
                                            <option value="0">{{trans('messages.disabled')}}</option>
                                        </select>
                                    </div>
                                </div>
                                --}}
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="input-label" for="">{{trans('messages.address')}}</label>
                                        <input type="text" name="address" class="form-control" placeholder="Ex : Lahore Pakistan"
                                               required>
                                    </div>
                                </div>
                                <div class="col-6 d-flex">
                                    <div class="form-group">
                                        <label class="input-label">{{trans('messages.delivery_time')}}</label>
                                        <div class="col-12 pr-10 pl-0">
                                            <input type="text" name="delivery_time" class="form-control" placeholder="delivery_time">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="input-label">{{trans('messages.delivery_charges')}}</label>
                                        <div class="col-12 pr-2 pl-0">
                                            <input type="text" name="delivery_charges" class="form-control" placeholder="delivery_charges">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label class="input-label">{{trans('messages.working_hours')}}</label>
                                        <div class="d-flex">
                                            <div class="col-6 pr-2 pl-0">
                                                <input type="time" name="start_time" class="form-control" placeholder="Start Time"
                                                       >
                                            </div>
                                            <div class="col-6 pl-2 pr-0">
                                                <input type="time" name="end_time" class="form-control" placeholder="End Time"
                                                       >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>{{trans('messages.logo')}}</label><small style="color: red">* ( {{trans('messages.ratio')}} 3:1 )</small>
                                        <div class="">
                                            <input type="file" accept=".jpg, .png, .jpeg" name="logo" id="logo" aria-describedby="fileHelp">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">{{trans('messages.submit')}}</button>
                        </form>
                    </div>
                </div>
                <!-- End Card -->
            </div>
        </div>
    </div>
    <!-- End Content -->
</main>
<!-- ========== END MAIN CONTENT ========== -->


<!-- JS Implementing Plugins -->
<script src="{{asset('assets/admin')}}/js/vendor.min.js"></script>
<script src="{{asset('assets/admin')}}/js/main.js"></script>

<!-- JS Front -->
<script src="{{asset('assets/admin')}}/js/theme.min.js"></script>
<script src="{{asset('assets/admin')}}/js/toastr.js"></script>
{!! Toastr::message() !!}

@if ($errors->any())
    <script>
        @foreach($errors->all() as $error)
        toastr.error('{{$error}}', Error, {
            CloseButton: true,
            ProgressBar: true
        });
        @endforeach
    </script>
@endif

<!-- JS Plugins Init. -->
<script>
    $(document).on('ready', function () {
        // INITIALIZATION OF SHOW PASSWORD
        // =======================================================
        $('.js-toggle-password').each(function () {
            new HSTogglePassword(this).init()
        });

        // INITIALIZATION OF FORM VALIDATION
        // =======================================================
        $('.js-validate').each(function () {
            $.HSCore.components.HSValidation.init($(this));
        });
    });
</script>
<!-- IE Support -->
<script>
    if (/MSIE \d|Trident.*rv:/.test(navigator.userAgent)) document.write('<script src="{{asset('assets/admin')}}/vendor/babel-polyfill/polyfill.min.js"><\/script>');
</script>
</body>
</html>
