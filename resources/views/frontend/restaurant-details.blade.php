@extends('layouts.site.app')

@section('title','Restaurant Details')

@push('css_or_js')

@endpush

@section('content')
<!--content wrapper-->
<div class="content-wrapper">
   <div class="container">
       <div class="restaurant-banner row">
           <!-- Restaurant Banner Left Starts -->
             <!--                    <div class="res-banner-left col-md-3">
             <div class="res-banner-img bg-img"
                  style="background-image: url(https://feedme.nc/images/shops/1596016193_avatar.jpg);"></div>
              </div>-->
              <!-- Restaurant Banner Left Ends -->
              <div class="banner-inner">
               <!-- Restaurant Banner Center Starts -->
               <div class="res-banner-center col-md-10">
                   <div class="row shop-details">
                       <div class="shop-detail-item col-md-4">
                        <h4 class="">{{ ucwords($restaurant->name) }}</h4>
                       </div>
                       <div class="shop-detail-item col-md-3">
                           {{ $restaurant->address }}
                       </div>
                       <div class="shop-detail-item col-md-2">
                           <div class="res-banner-ratings">
                               <div class="res-banner-rate-block">
                                   Ratings <br>

                                   <i class="ion-android-star ratingActive"></i>
                                   <i class="ion-android-star ratingActive"></i>
                                   <i class="ion-android-star ratingActive"></i>
                                   <i class="ion-android-star ratingActive"></i>
                                   <i class="ion-android-star ratingActive"></i>


                               </div>
                           </div>
                       </div>
                       <div class="shop-detail-item col-md-3">
                           Estimated delivery time <p class="">{{ $restaurant->delivery_time }}Mins</p>
                       </div>
                   </div>
                   <!-- <div class="col-md-12 shop-desc">
                       <p>wonderful food</p>
                   </div> -->

                   <!-- Restaurant Banner Ratings Ends -->

               </div>
               <!-- Restaurant Banner Center Ends -->
               <!-- Restaurant Banner Right Starts -->
           </div>
       </div>
   </div>
</div>
<!--cart-->
<div class="cart">
    <div class="food-section-outer">
        <div class="container">
{{--            <h2>Dynamic Tabs</h2>--}}
{{--            <p>To make the tabs toggleable, add the data-toggle="tab" attribute to each link. Then add a .tab-pane class with a unique ID for every tab and wrap them inside a div element with class .tab-content.</p>--}}
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home">Restaurant Products</a></li>
            <li><a data-toggle="tab" href="#menu1">Feature Products</a></li>
            <li><a data-toggle="tab" href="#menu2">Deals</a></li>
            {{--                <li><a data-toggle="tab" href="#menu3">Menu 3</a></li>--}}
        </ul>

        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="food-list-section row">
                    @forelse($featured_products as $fProduct)
                        <div class="col-lg-12">
                            <div class="card-inner">
                                <div class="card-s">
                                    <div class="img-set">
                                        <img src="{{ $fProduct->singleImage() }}" alt="">
                                    </div>
                                    <div class="food-detail">
                                        <div class="name-favourtbtn">
                                            <p>{{ $fProduct->name }}</p>
{{--                                            <p><button class="add-to-cartbtn Favourit"><i class="ion-heart"style="font-size: 20px;"></i></button></p>--}}
                                        </div>
                                        <div class="rating">
                                            <div class="res-banner-rate-block">
                                                <!-- <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i> -->
                                            </div>
                                        </div>
                                        <div class="prise-addbtn">
                                            <p class="price">${{ $fProduct->price }}</p>
{{--                                            <p><button class="add-to-cartbtn"><i class="ion-plus" style="font-size: 20px;"></i></button></p>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p style="padding-top: 22px;
    padding-left: 40px;">No Data Found</p>
                    @endforelse
            </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                @forelse($restaurant->products as $product)
                <div class="food-list-section row">
                    <div class="col-lg-12">
                        <div class="card-inner">
                            <div class="card-s">
                                <div class="img-set">
                                    <img src="{{ $product->singleImage() }}" alt="">
{{--                                    <img src="http://justhalaall.com/storage/product/1631288432613b7c70e2f44.png" alt="">--}}
                                </div>
                                <div class="food-detail">
                                    <div class="name-favourtbtn">
                                        <p>{{ $product->name }}</p>
{{--                                        <p><button class="add-to-cartbtn Favourit"><i class="ion-heart" style="font-size: 20px;"></i></button></p>--}}
                                    </div>
                                    <div class="rating">
                                        <div class="res-banner-rate-block">
                                            <!-- <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i> -->
                                        </div>
                                    </div>
                                    <div class="prise-addbtn">
                                        <p class="price">${{ $product->price }}</p>
                                        <p><button class="add-to-cartbtn"><i class="ion-plus" style="font-size: 20px;"></i></button></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @empty
                            <p>No Data Found</p>
                        @endforelse
                    </div>

                </div>
            </div>
            <div id="menu2" class="tab-pane fade">
                @forelse($restaurant->deals as $deal)
                    <div class="col-lg-12">
                        <div class="card-inner">
                            <div class="card-s">
                                <div class="img-set">
                                    <img src="{{ asset($deal->image) }}" alt="">
                                </div>
                                <div class="food-detail">
                                    <div class="name-favourtbtn">
                                        <p>{{ $deal->name }}</p>
{{--                                        <p><button class="add-to-cartbtn Favourit"><i class="ion-heart"style="font-size: 20px;"></i></button></p>--}}
                                    </div>
                                    <div class="rating">
                                        <div class="res-banner-rate-block">
                                            <!-- <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i> -->
                                        </div>
                                    </div>
                                    <div class="prise-addbtn">
                                        <p class="price">${{ $deal->price }}</p>
                                        <p><button class="add-to-cartbtn"><i class="ion-plus" style="font-size: 20px;"></i></button></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <p>No Data Found</p>
                @endforelse
            </div>
            <div id="menu3" class="tab-pane fade">
                <h3>Menu 3</h3>
                <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
            </div>
        </div>
    </div>
    <div class="col-md-3"></div>

</div>

        </div>
</div>
</div>
@endsection

@push('script')

@endpush
