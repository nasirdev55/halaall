@extends('layouts.site.app')

@section('title','Home')

@push('css_or_js')

@endpush

@section('content')
    <!-- banner  -->

    <div class="banner overflow-hidden">
        <div class="banner-slider">
            <div class="slide">
                <img src="{{ asset('assets/site/img/banner-bg.jpg')}}" alt="">
            </div>
            <div class="slide">
                <img src="{{ asset('assets/site/img/banner-bg.jpg')}}" alt="">

            </div>
        </div>
        <div class="container">
            <div class="row log-search-picker-row slider-search justify-content-center">
                <div class="col-sm-5 p-0 col-sm-offset-2">
                    <form action="{{ route('web.search') }}" method="GET">
                        @csrf
                        <div class="log-location-search input-group">
                            <input type="text" id="pac-input" name="term" class="form-control pac-target-input"
                                placeholder="Search Restaurant" autocomplete="off">
                            <span class="input-group-addon log-search-btn"><button type="submit">Find Restaurant</button></span>
                        </div>
                    </form>
                </div>
                <div class="col-sm-3"></div>
            </div>

        </div>
    </div>
    <!--all categories-->
    <div class="categorie">
        <div class="container">
            <h4>All Categories</h4>
            <div class="categorie-inner">
                <div class="categorie-pic">
                    @forelse ($data['categories'] as $category)
                        <li>
                            <a href="{{route('categoryDetail',['detail' => $category->id])}}">
                                <img src="{{ $category->image }}" alt="Img">
                                <span class="items-span">{{ $category->name }}</span>
                            </a>
                        </li>
                    @empty
                        <p>No Data Found</p>
                    @endforelse
                </div>

                <!--view all btn-->
                @if(count($data['categories']) >= 1)
                    <button class="view-all" data-toggle="modal" data-target="#categoriesListModal">View All</button>
                @endif
            </div>

        </div>
    </div>

    <!--Popular Restaurants card-->
    <div class="card-section">
        <div class="container">
            <h3>POPULAR</h3>
            <div class="card-inner" style="background: none !important;">

{{--                    @guest--}}
                @forelse($data['popular_restaurants'] as $pRestaurant)
                    <div class="card">
                        <a href="{{ route('web.restaurant.details', ['name' => $pRestaurant->name]) }}" class="food-item-box col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <div class="food-img bg-img">
                                <img src="{{asset('storage/restaurant/2021-08-14-6117b7fcef62d.png')}}" style="  height: 181px;
    width: 322px;">
                            </div>
                            <div class="food-details">
                                <h6 class="food-det-tit">{{ strtoupper($pRestaurant->name) }}</h6>
                                <!-- <p class="food-det-txt">wonderful food</p> -->
                                <div class="food-other-details row">
                                    <div class="col-xs-4 p-r-0 brdr-right">
                                        <span class="food-rating">
                                            Rating
                                            <br>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>
                                            <i class="ion-android-star ratingActive"></i>

                                        </span>
                                    </div>
                                    <div class="col-xs-8">
                                        <span class="food-deliver-time food-list-txt">
                                            Average Delivery Time
                                            <br>
                                            <span style="font-weight: 900;">
                                                {{$pRestaurant->delivery_time}}Mins
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @empty
                    <p>No Data Found</p>
                @endforelse
{{--                @endguest--}}
            </div>
        </div>
    </div>
    <!-- End Popular Restaurants card-->
    <div class="aside-backdrop"></div>
    <div class="aside right-aside location" id="login-sidebar">
        <div class="aside-header">
            <span class="close" data-dismiss="aside"><i class="ion-close-round"></i></span>
        </div>
        <div class="aside-contents" style="height: 597px;">
            <!-- Login Content Starts -->
            <div class="login-content">

                <!-- Login Form Section Starts -->
                <div class="login-form-sec">
                    <h5 class="login-tit">Login</h5>
                    <form action="{{ route('login') }}" method="post" role="form" class="popup-form" id="myLogin">
                        @csrf
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" id="login_password" name="email" placeholder="Enter Email"
                                class="form-control">
                            <div class="print-error-msg alert-danger error_email"></div>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" id="login_password" name="password" placeholder="Enter Password"
                                class="form-control">
                        </div>

                        <a href="" class="theme-link forgot-link">Forgot Your Password</a>
                        <button type="submit" class="login-btn login_btn">Login</button>
                    </form>
                </div>
                <!-- Login Form Section Ends -->
                <!-- Forgot Form Section Starts -->
                <div class="forgot-form-sec" style="display: none;">
                    <form class="form-horizontal" role="form">
                        <h5 class="login-tit">Forgot Your Password</h5>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" name="email" value="" required="" class="form-control">
                        </div>
                        <button type="submit" class="login-btn forgot-btn ">Submit</button>
                    </form>
                    <button type="button" class="forgot-btn login-btn login-link">Login</button>
                </div>
                <!-- Forgot Form Section Ends -->

            </div>
            <!-- Login Content Ends -->

        </div>
    </div>

    <div class="aside right-aside location" id="signup-sidebar" class="modal fade" role="dialog">
        <div class="aside-header">
            <span class="close" data-dismiss="aside"><i class="ion-close-round"></i></span>
        </div>
        <div class="aside-contents" style="height: 647px;">
            <!-- Login Head Starts -->
            <div class="login-head row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h5 class="login-tit">Sign Up</h5>
                </div>
            </div>
            <!-- Login Head Ends -->

            <!-- Login Content Starts -->
            <div class="login-content">
                <form action="{{ route('register') }}" method="post" class="popup-form" id="register_form">
                    @csrf
                    <div id="second_step">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" placeholder="">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Phone</label>
                            <input type="text" class="form-control @error('phone') is-invalid @enderror" id="phone" name="phone" value="{{ old('phone') }}" placeholder="">
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control @error('address') is-invalid @enderror" id="address" name="address" value="{{ old('address') }}" placeholder="">
                            @error('address')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group" id="password1">
                            <label>Password</label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group" id="password2">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" id="password_confirmation"
                                   name="password_confirmation">
                        </div>
                        <p class="signup-txt"> <input type="checkbox" value="1" id="check_2"
                                                      name="terms">
                            By creating an account, I accept the Terms &amp; Conditions</p>
                        <button type="submit" class="login-btn name-not-a-number register_btn">Sign Up</button>
                    </div>
                </form>
            </div>
            <!-- Login Content Ends -->
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="categoriesListModal" tabindex="-1" aria-labelledby="categoriesListModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <div class="modal-header">
            <h6 class="modal-title" id="categoriesListModalLabel">All Categories</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="categorie-inner categorie-list">
                <div class="categorie-pic">
                    @forelse ($data['categories'] as $category)
                        <li>
                            <a href="{{route('categoryDetail',['detail' => $category->id])}}">
                                <img src="{{ $category->image }}" alt="">
                                <span class="items-span">{{ $category->name }}</span>
                            </a>
                        </li>
                    @empty
                        <p>No Data Found</p>
                    @endforelse
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@push('script')

@endpush
