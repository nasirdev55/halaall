@extends('layouts.site.app')

@section('title','Terms and Conditions')

@section('content')
<div class="container">
    <div class="privacy">
        <h2 class="text-center">Terms and Conditions</h2>
        <div>
            <p>These terms of use (the Terms of Use) govern your use of our website www.evaly.com.bd (the Website) and our {{ config('app.name') }} application for mobile and handheld devices (the App). The Website and the App are jointly referred to as the Platform. Please read these Terms of Use carefully before you use the services. If you do not agree to these Terms of Use, you may not use the services on the Platform, and we request you to uninstall the App. By installing, downloading and/or even merely using the Platform, you shall be contracting with {{ config('app.name') }} and you provide your acceptance to the Terms of Use and other {{ config('app.name') }} policies (including but not limited to the Cancellation Refund Policy, Privacy Policy etc.) as posted on the Platform from time to time, which takes effect on the date on which you download, install or use the Services, and create a legally binding arrangement to abide by the same. The Platforms will be used by (i) natural persons who have reached 18 years of age and (ii) corporate legal entities, e.g companies. Where applicable, these Terms shall be subject to country-specific provisions as set out herein.</p>    
        </div>
            <div>
                <h3>USE OF PLATFORM AND SERVICES</h3>
                <p>All commercial/contractual terms are offered by and agreed to between Buyers and Merchants alone. The commercial/contractual terms include without limitation to price, taxes, shipping costs, payment methods, payment terms, date, period and mode of delivery, warranties related to products and services and after sales services related to products and services. {{ config('app.name') }} does not have any kind of control or does not determine or advise or in any way involve itself in the offering or acceptance of such commercial/contractual terms between the Buyers and Merchants. {{ config('app.name') }} may, however, offer support services to Merchants in respect to order fulfilment, payment collection, call centre, and other services, pursuant to independent contracts executed by it with the Merchants. eFood is not responsible for any non-performance or breach of any contract entered into between Buyers and Merchants on the Platform. eFood cannot and does not guarantee that the concerned Buyers and/or Merchants shall perform any transaction concluded on the Platform. eFood is not responsible for unsatisfactory services or non-performance of services or damages or delays as a result of products which are out of stock, unavailable or back ordered.</p>

                <p>{{ config('app.name') }} is operating an e-commerce platform and assumes and operates the role of facilitator, and does not at any point of time during any transaction between Buyer and Merchant on the Platform come into or take possession of any of the products or services offered by Merchant. At no time shall {{ config('app.name') }} hold any right, title or interest over the products nor shall {{ config('app.name') }} have any obligations or liabilities in respect of such contract entered into between Buyer and Merchant. You agree and acknowledge that we shall not be responsible for:</p>
                <ul>
                    <li>The goods provided by the shops or restaurants including, but not limited, serving of food orders suiting your requirements and needs;</li>
                    <li>The Merchant goods not being up to your expectations or leading to any loss, harm or damage to you;</li>
                    <li>The availability or unavailability of certain items on the menu;</li>
                    <li>The Merchant serving the incorrect orders.</li>
                </ul>

                <p>The details of the menu and price list available on the Platform are based on the information provided by the Merchants and we shall not be responsible for any change or cancellation or unavailability. All Menu  Food Images used on our platforms are only representative and shall/might not match with the actual Menu/Food Ordered, {{ config('app.name') }} shall not be responsible or Liable for any discrepancies or variations on this aspect.</p>
            </div>
    
            <div>
                <h3>Personal Information that you provide</h3>

                <p>If you want to use our service, you must create an account on our Site. To establish your account, we will ask for personally identifiable information that can be used to contact or identify you, which may include your name, phone number, and e-mail address. We may also collect demographic information about you, such as your zip code, and allow you to submit additional information that will be part of your profile. Other than basic information that we need to establish your account, it will be up to you to decide how much information to share as part of your profile. We encourage you to think carefully about the information that you share and we recommend that you guard your identity and your sensitive information. Of course, you can review and revise your profile at any time.</p>        
            </div>
        
            <div>
                <h3>Payment Information</h3>

                <p>To make the payment online for availing our services, you have to provide the bank account, mobile financial service (MFS), debit card, credit card information to the {{ config('app.name') }} platform.</p>        
            </div>
        

        <div>
            <h3>Session and Persistent Cookies</h3>

            <p>Cookies are small text files that are placed on your computer by websites that you visit. They are widely used in order to make websites work, or work more efficiently, as well as to provide information to the owners of the site. As is commonly done on websites, we may use cookies and similar technology to keep track of our users and the services they have elected. We use both session and persistent cookies. Session cookies are deleted after you leave our website and when you close your browser. We use data collected with session cookies to enable certain features on our Site, to help us understand how users interact with our Site, and to monitor at an aggregate level Site usage and web traffic routing. We may allow business partners who provide services to our Site to place cookies on your computer that assist us in analyzing usage data. We do not allow these business partners to collect your personal information from our website except as may be necessary for the services that they provide.</p>    
        </div>

        <div>
            <h3>CANCELLATION</h3>
            <p>{{ config('app.name') }} can cancel any order anytime due to the foods/products unavailability, out of coverage area and any other unavoidable circumstances.</p>
        </div>
    </div>
</div>
@endsection