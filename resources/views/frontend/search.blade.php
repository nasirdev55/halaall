@extends('layouts.site.app')

@section('title','Search')

@push('css_or_js')

@endpush

@section('content')
<div class="search-section">
    <div class="container">
        <div class="row">
            <div class="food-search-in">
                
                    <form style="width:100%;" id="searchForm" action="{{ route('web.search') }}" method="GET">
                        @csrf
                        <div class="res-banner-search input-group search-box search-set ser-food">
                        <!-- <div class="input-group"> -->
                        <!-- <span class="input-group-addon prodsearch"><i class="ion-location"
                                style="color: rgb(239, 120, 34); font-size: 20px;"></i></span> -->
                        <input type="text" id="searchbar" name="term" class="form-control  " placeholder="Search products, restaurants" value="{{ request()->input('term') }}">
                        <span class="input-group-addon prodsearch"><i class="ion-android-search"
                                style="color:#3d4152; font-size: 20px;"></i></span>    
                    </form>
                    </div>
                    <!-- <i class="ion-android-close search-close"></i> ESC -->
                    <!-- </div> -->

                
                <button class="cancel-food-search">Cancel</button>
            </div>
        </div>
        <div class="row">
            <ul class="nav nav-tabs food-tabss">
                <li class="active"><a data-toggle="tab" href="#menu1">Products</a></li>
                <li><a data-toggle="tab" href="#menu2">Restaurants</a></li>
            </ul>

            <div class="tab-content">

                <div id="menu1" class="tab-pane fade in active">
                    @forelse($data['products'] as $product)
                        <div class="col-md-6">
                            <div class="card-inner">
                                <div class="card-s">
                                    <div class="img-set">
                                        <img src="{{ $product->singleImage() }}" alt="">
                                    </div>
                                    <div class="food-detail">
                                        <div class="name-favourtbtn">
                                            <p>{{ $product->name }}</p>
                                            <p><button class="add-to-cartbtn Favourit"><i class="ion-heart"
                                                        style="font-size: 20px;"></i></button></p>
                                        </div>
                                        <div class="rating">
                                            <div class="res-banner-rate-block">
                                                <!-- <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i>
                                                <i class="ion-android-star ratingActive"></i> -->
                                            </div>
                                        </div>
                                        <div class="prise-addbtn">
                                            <p class="price">${{ $product->price }}</p>
                                            <p><button class="add-to-cartbtn"><i class="ion-plus"
                                                        style="font-size: 20px;"></i></button></p>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <img src="{{ asset('assets/site/img/app/no-data-found.svg') }}" style="max-width: 250px;">
                    @endforelse
                </div>
                <div id="menu2" class="tab-pane fade">
                    @forelse($data['restaurants'] as $restaurant)
                        <a href="{{ route('web.restaurant.details', ['name' => $restaurant->name])  }}">
                            <div class="col-md-6">
                                <div class="card-inner">
                                    <div class="card-s">
                                        <div class="img-set">
                                            <img src="{{ $restaurant->logo }}" alt="">
                                        </div>
                                        <div class="food-detail">
                                            <div class="name-favourtbtn">
                                                <p>{{ $restaurant->name }}</p>
                                                <p><button class="add-to-cartbtn Favourit"><i class="ion-heart"
                                                            style="font-size: 20px;"></i></button></p>
                                            </div>
                                            <div class="rating">
                                                <div class="res-banner-rate-block">

                                                    <i class="ion-android-star ratingActive"></i>
                                                    <i class="ion-android-star ratingActive"></i>
                                                    <i class="ion-android-star ratingActive"></i>
                                                    <i class="ion-android-star ratingActive"></i>
                                                    <i class="ion-android-star ratingActive"></i>


                                                </div>
                                            </div>
                                            <div class="prise-addbtn">
                                                <p class="price">${{ $restaurant->delivery_charges }}</p>
                                                <p><button class="add-to-cartbtn"><i class="ion-plus"
                                                            style="font-size: 20px;"></i></button></p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @empty
                        <img src="{{ asset('assets/site/img/app/no-data-found.svg') }}" style="max-width: 250px;">
                    @endforelse
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@push('script')

@endpush
