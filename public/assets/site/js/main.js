$(document).ready(function () {


    $('.banner-slider').slick();

    $(".signinform").on("click", function () {
        $("#login-sidebar").addClass("in")
        $(".aside-backdrop").addClass("in")
    })
    $(".close").on("click", function () {
        $("#login-sidebar").removeClass("in")
        $(".aside-backdrop").removeClass("in")
        $("#signup-sidebar").removeClass("in")

    })

    $(".forgot-link").on("click", function (e) {
        e.preventDefault()
        $(".login-form-sec").hide()
        $(".forgot-form-sec").fadeIn()
    })
    $(".login-link").on("click", function (e) {
        e.preventDefault()
        $(".login-form-sec").fadeIn()
        $(".forgot-form-sec").hide()
    })

    $(".signupform").on("click", function (e) {
        e.preventDefault()
        $("#signup-sidebar").addClass("in")
        $(".aside-backdrop").addClass("in")

    })

    $("#searchbar").on("keypress", function(e){
        if(e && e.keyCode == 13) {
          $("#searchForm").submit();
        }    
    })
    

    // $('.login_btn').on('click',function(){
    //     var email       = document.getElementById("email").value;
    //     var password    = document.getElementById("login_password").value;
    //     $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //     });

    //     $.ajax({
    //         url: '{{route("login")}}',
    //         type:'POST',
    //         data:{ email : email,password:password},
    //         success: function(data) { 
    //             console.log(data);
    //             return false;
    //         },
    //         error:function(jqXhr,status) { 
    //             if(jqXhr.status === 422) {
    //                 $("#myLogin .print-error-msg").html('');
    //                 $("#myLogin .print-error-msg").show();
    //                 var errors = jqXhr.responseJSON;
    //                 console.log(errors);
    //                 $.each( errors , function( key, value ) { 
    //                     $("#myLogin").find(".error_"+key).html(value);
    //                 }); 
    //             } 
    //         }
    //     });
    // });

    $('.cancel-food-search').on('click', function() {
        $('#searchbar').val('');
    });
});