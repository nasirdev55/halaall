"use strict";

jQuery(document).on('ready', function () {
	jQuery('#res-register').on('click', function(){
		jQuery('#do-restaurant-login').addClass('res-hide-form');
		jQuery('#res-register-form').removeClass('res-hide-form');
		jQuery('#dynamo').removeClass('col-lg-5').addClass('col-lg-10');
	});
});